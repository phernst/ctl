# Qt specific
QT += testlib
QT -= gui
CONFIG -= app_bundle
DEFINES += QT_DEPRECATED_WARNINGS \
           QT_NO_DEBUG_OUTPUT \
           QT_FORCE_ASSERTS

# Compiler optimization
CONFIG += optimize_full

# Warnings as errors
CONFIG += warn_on
win32-g++|!win32: QMAKE_CXXFLAGS += -Werror -Wall -Wextra -Wnon-virtual-dtor -Wold-style-cast -Wpedantic -Woverloaded-virtual -Wformat=2 -Wcast-align
win32-msvc*: QMAKE_CXXFLAGS += /WX

# CTL MODULES
include(../../modules/ctl.pri)
include(../../modules/ctl_ocl.pri)

# Source code of unit tests
SOURCES += \
    datamodeltest.cpp \
    errormetrictest.cpp \
    main.cpp \
    acquisitionsetuptest.cpp \
    ctsystemtest.cpp \
    datatypetest.cpp \
    denfileiotest.cpp \
    fouriertest.cpp \
    geometrytest.cpp \
    nrrdfileiotest.cpp \
    projectionfiltertest.cpp \
    projectionmatrixtest.cpp \
    projectortest.cpp \
    reconstructiontest.cpp \
    spectrumtest.cpp \
    subsetgeneratortest.cpp \
    volumefiltertest.cpp

HEADERS += \
    acquisitionsetuptest.h \
    ctsystemtest.h \
    datamodeltest.h \
    datatypetest.h \
    denfileiotest.h \
    errormetrictest.h \
    fouriertest.h \
    geometrytest.h \
    nrrdfileiotest.h \
    projectionfiltertest.h \
    projectionmatrixtest.h \
    projectortest.h \
    reconstructiontest.h \
    spectrumtest.h \
    subsetgeneratortest.h \
    volumefiltertest.h

# Copy test data
win32:QMAKE_POST_LINK += $(COPY_DIR) \"$$shell_path($$PWD/../testData)\" \"$$shell_path($$DESTDIR/testData)\" $$escape_expand(\n\t)
!win32:QMAKE_POST_LINK += $(COPY_DIR) \"$$shell_path($$PWD/../testData)\" \"$$shell_path($$DESTDIR)\" $$escape_expand(\n\t)
