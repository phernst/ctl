#include "datamodeltest.h"

#include "io/jsonserializer.h"
#include "models/datamodels1d.h"
#include "models/datamodels2d.h"
#include "models/datamodeloperations.h"
#include "models/tabulateddatamodel.h"
#include "models/xrayspectrummodels.h"

using namespace CTL;

DataModelTest::DataModelTest()
{
}

void DataModelTest::initTestCase()
{
    _samples1D.append(-10.0f);
    _samples1D.append(-5.0f);
    _samples1D.append(-0.0f);
    _samples1D.append(5.0f);
    _samples1D.append(10.0f);

    _samples1DPositive.append(0.1337f);
    _samples1DPositive.append(4.0f);
    _samples1DPositive.append(10.2f);
    _samples1DPositive.append(15.0f);
    _samples1DPositive.append(21.4f);

    _samples2D.append( { -5.0f, -10.0f } );
    _samples2D.append( { -5.0f,  10.0f } );
    _samples2D.append( {  0.0f,   0.0f } );
    _samples2D.append( {  5.0f,   5.0f } );
    _samples2D.append( {  5.0f,  -5.0f } );
    _samples2D.append( { 10.0f,   5.0f } );
}

void DataModelTest::testSerialization1DModels()
{
    {
        qInfo() << "IdentityModel";
        const auto model_defCtor = std::make_shared<IdentityModel>();
        runSerializationTest(model_defCtor, _samples1D);
    }
    {
        qInfo() << "ConstantModel";
        const auto model = std::make_shared<ConstantModel>(13.37f);
        runSerializationTest(model, _samples1D);

        const auto model_defCtor = std::make_shared<ConstantModel>();
        runSerializationTest(model_defCtor, _samples1D);
    }
    {
        qInfo() << "SaturatedLinearModel";
        const auto model = std::make_shared<SaturatedLinearModel>(-1.3f, 3.7f);
        runSerializationTest(model, _samples1D);

        const auto model_defCtor = std::make_shared<SaturatedLinearModel>();
        runSerializationTest(model_defCtor, _samples1D);
    }
    {
        qInfo() << "SaturatedSplineModel";
        const auto model = std::make_shared<SaturatedSplineModel>(-42.0f, 42.0f, 3.0f, 2.5f);
        runSerializationTest(model, _samples1D);

        const auto model_defCtor = std::make_shared<SaturatedSplineModel>();
        runSerializationTest(model_defCtor, _samples1D);
    }
    {
        qInfo() << "RectFunctionModel";
        const auto model = std::make_shared<RectFunctionModel>(-13.37f, 42.0f, 6.66f);
        runSerializationTest(model, _samples1D);

        const auto model_defCtor = std::make_shared<RectFunctionModel>();
        runSerializationTest(model_defCtor, _samples1D);
    }
    {
        qInfo() << "GaussianModel1D";
        const auto model = std::make_shared<GaussianModel1D>(4.6f, 42.0f, 1.337f);
        runSerializationTest(model, _samples1D);

        const auto model_defCtor = std::make_shared<GaussianModel1D>();
        runSerializationTest(model_defCtor, _samples1D);
    }
    {
        qInfo() << "TabulatedDataModel";
        const auto model = std::make_shared<TabulatedDataModel>();
        model->insertDataPoint(-43.0f, 234.6f);
        model->insertDataPoint(-3.0f, -21.1f);
        model->insertDataPoint(13.0f, 13.3f);
        model->insertDataPoint(25.0f, -7.7f);
        runSerializationTest(model, _samples1D);

        const auto model_defCtor = std::make_shared<TabulatedDataModel>();
        runSerializationTest(model_defCtor, _samples1D);
    }
    {
        qInfo() << "XrayLaserSpectrumModel";
        const auto model = std::make_shared<XrayLaserSpectrumModel>();
        model->setParameter(13.37);
        runSerializationTest(model, _samples1DPositive);

        const auto model_defCtor = std::make_shared<XrayLaserSpectrumModel>();
        runSerializationTest(model_defCtor, _samples1DPositive);
    }
    {
        qInfo() << "FixedXraySpectrumModel";
        auto table = TabulatedDataModel();
        table.insertDataPoint(3.0f, 234.6f);
        table.insertDataPoint(5.0f, 21.1f);
        table.insertDataPoint(13.0f, 13.3f);
        table.insertDataPoint(25.0f, 7.7f);
        const auto model = std::make_shared<FixedXraySpectrumModel>(table);
        runSerializationTest(model, _samples1DPositive);

        const auto model_defCtor = std::make_shared<FixedXraySpectrumModel>();
        runSerializationTest(model_defCtor, _samples1DPositive);
    }
    {
        qInfo() << "KramersLawSpectrumModel";
        const auto model = std::make_shared<KramersLawSpectrumModel>();
        model->setParameter(13.37);
        runSerializationTest(model, _samples1DPositive);

        const auto model_defCtor = std::make_shared<KramersLawSpectrumModel>();
        runSerializationTest(model_defCtor, _samples1DPositive);
    }
    {
        qInfo() << "HeuristicCubicSpectrumModel";
        const auto model = std::make_shared<HeuristicCubicSpectrumModel>();
        model->setParameter(13.37);
        runSerializationTest(model, _samples1DPositive);

        const auto model_defCtor = std::make_shared<HeuristicCubicSpectrumModel>();
        runSerializationTest(model_defCtor, _samples1DPositive);
    }
    {
        qInfo() << "TASMIPSpectrumModel";
        const auto model = std::make_shared<TASMIPSpectrumModel>(77.7f);
        runSerializationTest(model, _samples1DPositive);

        const auto model_defCtor = std::make_shared<TASMIPSpectrumModel>();
        runSerializationTest(model_defCtor, _samples1DPositive);
    }
}

void DataModelTest::testSerialization2DModels()
{
    {
        qInfo() << "ConstantModel2D";
        const auto model = std::make_shared<ConstantModel2D>(13.37f);
        runSerializationTest(model, _samples2D);

        const auto model_defCtor = std::make_shared<ConstantModel2D>();
        runSerializationTest(model_defCtor, _samples2D);
    }
    {
        qInfo() << "RectModel2D";
        const auto model = std::make_shared<RectModel2D>(1.337f, -42.0f, 32.0f, -1.0f, 4.0f);
        runSerializationTest(model, _samples2D);

        const auto model_defCtor = std::make_shared<RectModel2D>();
        runSerializationTest(model_defCtor, _samples2D);
    }
    {
        qInfo() << "GaussianModel2D";
        const auto model = std::make_shared<GaussianModel2D>(4.6f, 42.0f, -42.0f, 1.337f, 2.0f, 0.25f);
        runSerializationTest(model, _samples2D);

        const auto model_defCtor = std::make_shared<GaussianModel2D>();
        runSerializationTest(model_defCtor, _samples2D);
    }
    {
        qInfo() << "SeparableProductModel";
        const auto model1 = std::make_shared<GaussianModel1D>(4.6f, 42.0f, 1.337f);
        const auto model2 = std::make_shared<RectFunctionModel>(-13.37f, 42.0f, 6.66f);
        const auto model = std::make_shared<SeparableProductModel>(model1, model2);
        runSerializationTest(model, _samples2D);
    }
}

void DataModelTest::testSerializationModelOperations1D()
{
    const auto model1 = std::make_shared<GaussianModel1D>(4.6f, 42.0f, 1.337f);
    const auto model2 = std::make_shared<RectFunctionModel>(-13.37f, 42.0f, 6.66f);

    {
        qInfo() << "1D Add";
        const auto model = model1 + model2;
        runSerializationTest(model, _samples1D);
    }
    {
        qInfo() << "1D Sub";
        const auto model = model1 - model2;
        runSerializationTest(model, _samples1D);
    }
    {
        qInfo() << "1D Mul";
        const auto model = model1 * model2;
        runSerializationTest(model, _samples1D);
    }
    {
        qInfo() << "1D Div";
        const auto model = model1 / model2;
        runSerializationTest(model, _samples1D);
    }
    {
        qInfo() << "1D Cat";
        const auto model = model1 | model2;
        runSerializationTest(model, _samples1D);
    }
}

void DataModelTest::testSerializationModelOperations2D()
{
    const auto model1 = std::make_shared<RectModel2D>(1.337f, -42.0f, 32.0f, -1.0f, 4.0f);
    const auto model2 = std::make_shared<GaussianModel2D>(4.6f, 42.0f, -42.0f, 133.7f, 200.0f, 0.25f);

    {
        qInfo() << "2D Add";
        const auto model = model1 + model2;
        runSerializationTest(model, _samples2D);
    }
    {
        qInfo() << "2D Sub";
        const auto model = model1 - model2;
        runSerializationTest(model, _samples2D);
    }
    {
        qInfo() << "2D Mul";
        const auto model = model1 * model2;
        runSerializationTest(model, _samples2D);
    }
    {
        qInfo() << "2D Div";
        const auto model = model1 / model2;
        runSerializationTest(model, _samples2D);
    }
}

void DataModelTest::runSerializationTest(std::shared_ptr<const CTL::AbstractDataModel> model, const QVector<float> &samplingPts) const
{
    const float eps = 1.0e-6f;
    static const QString fileName = QStringLiteral("model1d.json");

    JsonSerializer ser;
    ser.serialize(*model, fileName);

    auto model_deserialized = ser.deserializeDataModel(fileName);

    const auto probeValue = [&model, &model_deserialized, eps] (float pos)
    {
        return qAbs(model->valueAt(pos) - model_deserialized->valueAt(pos)) < eps;
    };

    for(const auto& pt : samplingPts)
        QVERIFY(probeValue(pt));
}

void DataModelTest::runSerializationTest(std::shared_ptr<const CTL::AbstractDataModel2D> model, const QVector<QPair<float, float> > &samplingPts) const
{
    const float eps = 1.0e-5f;
    static const QString fileName = QStringLiteral("model2d.json");

    JsonSerializer ser;
    ser.serialize(*model, fileName);

    auto model_deserialized = ser.deserializeDataModel2D(fileName);

    const auto probeValue = [&model, &model_deserialized, eps] (float posX, float posY)
    {
        return qAbs(model->valueAt(posX, posY) - model_deserialized->valueAt(posX, posY)) < eps;
    };

    for(const auto& pt : samplingPts)
        QVERIFY(probeValue(pt.first, pt.second));
}
