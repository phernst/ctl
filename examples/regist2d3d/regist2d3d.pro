QT -= gui
CONFIG += cmdline

SOURCES += main.cpp

# CTL modules
include(../../modules/ctl.pri)
include(../../modules/ctl_ocl.pri)
include(../../modules/ctl_nlopt.pri)
