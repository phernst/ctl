#include "simplesubsetgenerator.h"

#include "mat/deg.h"

#include <QDebug>
#include <QMetaEnum>

namespace CTL {

DECLARE_SERIALIZABLE_TYPE(SimpleSubsetGenerator)

namespace  {
std::vector<uint> valuesToIndices(const std::vector<float>& values);
std::vector<uint> bestOrthoOrder180(int nbAngles);
std::vector<uint> bestOrthoOrder360(int nbAngles);
}

/*!
 * \brief  Creates a SimpleSubsetGenerator that generates \a nbSubsets subsets according to the
 * creation pattern \a subsetOrder.
 *
 *  Example:
 * \code
 *  // create dummy projection data (sizes entirely arbitrary, and irrelevant here)
 *  ProjectionData dummy(1,1,1);
 *  dummy.allocateMemory(10);   // 'dummy' now contains 10 views
 *
 *  // create a subset generator with 3 subsets and the pattern to 'Adjacent'
 *  SimpleSubsetGenerator subsetGen(3, SimpleSubsetGenerator::Adjacent);
 *
 *  // pass the dummy projection data (note: it contains 36 views)
 *  subsetGen.setProjections(dummy);
 *
 *  // generate subsets (note: iteration '0' is arbitrary and has no effect here)
 *  const auto subsets = subsetGen.generateSubsets(0);
 *
 *  // print the ids of the views included in the two subsets
 *  for(const auto& subset : subsets)
 *      qInfo() << subset.viewIds();
 *
 *      // output:
 *      // std::vector(18, 27, 22, 31, 25, 34, 20, 29, 21)
 *      // std::vector(30, 19, 28, 26, 35, 23, 32, 24, 33)
 *      // std::vector(0, 9, 4, 13, 7, 16, 2, 11, 3)
 *      // std::vector(12, 1, 10, 8, 17, 5, 14, 6, 15)
 * \endcode
 */
SimpleSubsetGenerator::SimpleSubsetGenerator(uint nbSubsets, Order subsetOrder)
    : _order(subsetOrder)
{
    setNbSubsets(nbSubsets);
}

/*!
 * \brief Sets the order used for selecting views for a subset to \a order.
 *
 * The following options of subset generation patterns are available:
 * - Order::Random        - randomly selects views for a subset
 * - Order::Adjacent      - puts adjacent views into a subset
 * - Order::Orthogonal180 - selects sets of most orthogonal views, assuming the total scan range was 180 degrees
 * - Order::Orthogonal360 - selects sets of most orthogonal views, assuming the total scan range was 360 degrees
 *
 * Examples:
 * see generateSubsetsImpl().
 */
void SimpleSubsetGenerator::setOrder(SimpleSubsetGenerator::Order order) { _order = order; }

// use base class documentation
void SimpleSubsetGenerator::fromVariant(const QVariant& variant)
{
    AbstractFixedSizeSubsetGenerator::fromVariant(variant);

    auto varMap = variant.toMap();

    auto orderID = metaEnum().keyToValue(qPrintable(varMap.value(QStringLiteral("subset order")).toString()));
    if(orderID == -1)
    {
        qWarning() << "Improper subset order type (valid: Random, Adjacent, Orthogonal180, "
                      "Orthogonal360). Using default value (Random).";
        orderID = Random;
    }
    setOrder(static_cast<Order>(orderID));
}

// use base class documentation
QVariant SimpleSubsetGenerator::toVariant() const
{
    QVariantMap ret = AbstractFixedSizeSubsetGenerator::toVariant().toMap();

    ret.insert(QStringLiteral("subset order"), metaEnum().valueToKey(_order));

    return ret;
}

/*!
 * \brief Creates subsets based on random selection of views.
 *
 * Ensures that each view appears only once in the entire set of subsets.
 */
std::vector<ProjectionDataView>
SimpleSubsetGenerator::makeRandomSubsets(const std::vector<uint>& subsetSizes) const
{
    auto shuffledProj = _fullProjections.shuffled(_rng());

    std::vector<ProjectionDataView> subsets;
    subsets.reserve(subsetSizes.size());

    uint cnt = 0;
    for(auto size : subsetSizes)
    {
        subsets.push_back(shuffledProj.subset(cnt, size));
        cnt += size;
    }

    return subsets;
}

/*!
 * \brief Creates subsets taking adjacent blocks of views into a subset.
 *
 * Ensures that each view appears only once in the entire set of subsets.
 */
std::vector<ProjectionDataView>
SimpleSubsetGenerator::makeAdjacentSubsets(const std::vector<uint>& subsetSizes) const
{
    std::vector<ProjectionDataView> subsets;
    subsets.reserve(subsetSizes.size());

    uint cnt = 0;
    for(auto size : subsetSizes)
    {
        subsets.push_back(_fullProjections.subset(cnt, size));
        cnt += size;
    }

    return subsets;
}

/*!
 * \brief Creates subsets based on the assumption of a circular scan orbit.
 *
 * Assumes a total scan range of either 180 or 360 degrees, depending on what has been specified as
 * order for the subsets (see setOrder()).
 *
 * Ensures that each view appears only once in the entire set of subsets.
 */
std::vector<ProjectionDataView>
SimpleSubsetGenerator::makeOrthogonalSubsets(const std::vector<uint>& subsetSizes) const
{
    std::vector<ProjectionDataView> subsets;
    subsets.reserve(subsetSizes.size());

    const auto viewOrder = (_order == Orthogonal180) ? bestOrthoOrder180(_fullProjections.nbViews())
                                                     : bestOrthoOrder360(_fullProjections.nbViews());

    auto idIter = viewOrder.cbegin();
    for(auto size : subsetSizes)
    {
        const std::vector<uint> ids(idIter, idIter + size);
        subsets.push_back(_fullProjections.subset(ids));
        idIter += size;
    }

    return subsets;
}

/*!
 * \brief QMetaEnum object used to decode/encode subset order enum values.
 */
QMetaEnum SimpleSubsetGenerator::metaEnum()
{
    const auto& mo = SimpleSubsetGenerator::staticMetaObject;
    const auto idx = mo.indexOfEnumerator("Order");
    return mo.enumerator(idx);
}

/*!
 * \brief Implementation of the subset generation routine.
 *
 * This generates the subsets for the data that have been set through setProjections() (or
 * setData()). Generates the same subsets independent of the \a iteration input.
 *
 * The generation pattern used depends on the mode selected in the constructor or through setOrder():
 * - Order::Random        - randomly selects views for a subset
 * - Order::Adjacent      - puts adjacent views into a subset
 * - Order::Orthogonal180 - selects sets of most orthogonal views, assuming the total scan range was 180 degrees
 * - Order::Orthogonal360 - selects sets of most orthogonal views, assuming the total scan range was 360 degrees
 *
 * Example:
 * \code
 *  // create dummy projection data (sizes entirely arbitrary, and irrelevant here)
 *  ProjectionData dummy(1,1,1);
 *  dummy.allocateMemory(10);   // 'dummy' now contains 10 views
 *
 *  // create a subset generator and set the number of subsets to 2
 *  SimpleSubsetGenerator subsetGen;
 *  subsetGen.setNbSubsets(2);
 *
 *  // pass the dummy projection data (note: it contains 10 views)
 *  subsetGen.setProjections(dummy);
 *
 *  // we use a simple function to try out the different generation patterns
 *  const auto runGeneration = [&subsetGen] (const SimpleSubsetGenerator::Order& order)
 *  {
 *      // set the subset order
 *      subsetGen.setOrder(order);
 *
 *      // generate subsets (note: iteration '0' is arbitrary and has no effect here)
 *      const auto subsets = subsetGen.generateSubsets(0);
 *
 *      // print the ids of the views included in the two subsets
 *      for(const auto& subset : subsets)
 *          qInfo() << subset.viewIds();
 *  };
 *
 *  // order: Random
 *  runGeneration(SimpleSubsetGenerator::Random);
 *
 *      // output:
 *      // std::vector(6, 2, 1, 0, 3)
 *      // std::vector(8, 7, 9, 4, 5)
 *
 *  // order: Adjacent
 *  runGeneration(SimpleSubsetGenerator::Adjacent);
 *
 *      // output:
 *      // std::vector(0, 1, 2, 3, 4)
 *      // std::vector(5, 6, 7, 8, 9)
 *
 *  // order: Orthogonal180
 *  runGeneration(SimpleSubsetGenerator::Orthogonal180);
 *
 *      // output:
 *      // std::vector(9, 1, 6, 2, 7)
 *      // std::vector(0, 5, 3, 8, 4)
 *
 *  // order: Orthogonal360
 *  runGeneration(SimpleSubsetGenerator::Orthogonal360);
 *
 *      // output:
 *      // std::vector(0, 3, 1, 4, 2)
 *      // std::vector(5, 8, 6, 9, 7)
 * \endcode
 */
std::vector<ProjectionDataView> SimpleSubsetGenerator::generateSubsetsImpl(uint) const
{  
    if(_fullProjections.nbViews() == 0)
    {
        qCritical() << "Could not generate subsets. No projection data set or data has zero views.";
        return {};
    }

    std::vector<ProjectionDataView> ret;

    auto subsetSizes = assist::subsetSizes(_fullProjections.nbViews(), _nbSubsets);

    switch (_order) {
    case CTL::SimpleSubsetGenerator::Random:
        ret = makeRandomSubsets(subsetSizes);
        break;
    case CTL::SimpleSubsetGenerator::Adjacent:
        ret = makeAdjacentSubsets(subsetSizes);
        break;
    case CTL::SimpleSubsetGenerator::Orthogonal180:
        Q_FALLTHROUGH();
    case CTL::SimpleSubsetGenerator::Orthogonal360:
        ret = makeOrthogonalSubsets(subsetSizes);
        break;
    }

    if(_permuteSubsets)
        std::shuffle(ret.begin(), ret.end(), _rng);

    return ret;
}

namespace  {

/*!
 * \brief Returns a vector of indices telling the position, a value in 'values' would have in a
 * sorted list.
 *
 * Example: an index of 3 (ie. element number 4) in the result means, the corresponding value is the
 * fourth smallest in 'values'
 *
 * \code
 *  std::vector<float> vec{ 42.0f, 1.0f, 13.37f, -1.337f, 3.141f };
 *
 *  qInfo() << valuesToIndices(vec);
 *  // output: std::vector(4, 1, 3, 0, 2)
 *  \endcode
 */
std::vector<uint> valuesToIndices(const std::vector<float>& values)
{
    std::vector<float> sorted(values);
    std::sort(sorted.begin(), sorted.end());

    std::vector<uint> idx(values.size());
    for(std::size_t i = 0; i < idx.size(); ++i)
    {
        // find position of the i'th value from the original list in the sorted list
        const auto posInOriginal = std::find(sorted.cbegin(), sorted.cend(), values[i]);
        idx[i] = static_cast<uint>(std::distance(sorted.cbegin(), posInOriginal));
    }

    return idx;
}

/*!
 * \brief Returns a vector containing the indices of views sorted by their orthogonality under the
 * assumption that the total number of angles is \a nbAngles and they are distributed over a 180
 * degree circular orbit.
 *
 * Example:
 * \code
 *  qInfo() << bestOrthoOrder180(18);
 *  // output: std::vector(0, 9, 4, 13, 7, 16, 2, 11, 3, 12, 1, 10, 8, 17, 5, 14, 6, 15)
 * \endcode
 */
std::vector<uint> bestOrthoOrder180(int nbAngles)
{
    if(nbAngles <= 0)
        throw std::domain_error("Orthogonal subset generation requires a positive number of views.");

    if(nbAngles == 1)
        return { 0 };

    const auto size = int(std::pow(2.0f,int(std::ceil(std::log(float(nbAngles)/2.0f)/std::log(2.0f)))));

    std::vector<float> relPos(size);
    relPos[0] = 0.0f;
    if (nbAngles > 2) relPos[1] = 0.5f;

    int n = 0, first = 1;
    float step = 0.25f;
    while (first < nbAngles/2)
    {
        const auto last = static_cast<int>(std::pow(2.0f, n));
        for(int j = 0; j < last; ++j)
        {
            relPos[first+2*j+1] = relPos[first-j] + step;
            relPos[first+2*j+2] = relPos[first-j] - step;
        }
        first += 2 * last;
        step /= 2.0f;
        n++;
    }

    std::vector<float> z(nbAngles);
    for(int i = 0; i <= (nbAngles-1)/2; ++i)
    {
        z[2*i] = relPos[i];
        if(2*i+1 < nbAngles)
            z[2*i+1] = relPos[i] + 1.0f;
    }

    return valuesToIndices(z);
}

/*!
 * \brief Returns a vector containing the indices of views sorted by their orthogonality under the
 * assumption that the total number of angles is \a nbAngles and they are distributed over a 360
 * degree circular orbit.
 *
 * Example:
 * \code
 *  qInfo() << bestOrthoOrder360(18);
 *  // output: std::vector(0, 5, 3, 7, 4, 8, 1, 6, 2, 9, 14, 12, 16, 13, 17, 10, 15, 11)
 * \endcode
 */
std::vector<uint> bestOrthoOrder360(int nbAngles)
{
    // compute first half using 180deg version
    const auto halfSize  = int(std::ceil(nbAngles / 2.0f));
    auto firstHalf = bestOrthoOrder180(halfSize);

    // resize vector and compute second half as shifted version of first
    auto& fullSet = firstHalf;
    fullSet.resize(nbAngles);
    for(int i = halfSize; i < nbAngles; ++i)
        fullSet[i] = firstHalf[i - halfSize] + halfSize;

    return fullSet;
}

} // anonymous namespace

/*!
 * \enum SimpleSubsetGenerator::Order
 * Enumeration for subset selection orders.
 *
 * The selection order decides which views will end up in a particular subset.
 */

/*!
 * \var SimpleSubsetGenerator::Random
 *
 * Randomly selects views for a subset.
 */

/*!
 * \var SimpleSubsetGenerator::Adjacent
 *
 * Puts adjacent views into a subset.
 */

/*!
 * \var SimpleSubsetGenerator::Orthogonal180
 *
 * Selects sets of most orthogonal views, assuming a circular scan orbit with an agular range of
 * 180 degrees.
 */

/*!
 * \var SimpleSubsetGenerator::Orthogonal360
 *
 * Selects sets of most orthogonal views, assuming a circular scan orbit with an agular range of
 * 360 degrees.
 */

} // namespace CTL
