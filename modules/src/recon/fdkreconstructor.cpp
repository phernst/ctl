#include "fdkreconstructor.h"
#include "acquisition/geometryencoder.h"
#include "components/abstractdetector.h"
#include "components/abstractgantry.h"
#include "img/voxelvolume.h"
#include "mat/deg.h"
#include "mat/matrix_algorithm.h"
#include "mat/pi.h"
#include "processing/oclprojectionfilters.h"
#include "recon/sfpbackprojector.h"
#include "recon/simplebackprojector.h"

#include <set>

namespace CTL {
namespace OCL {

DECLARE_SERIALIZABLE_TYPE(FDKReconstructor)

static assist::Circle3D circleFromSetup(AcquisitionSetup& setup);

/*!
 * \brief Passes the AcquisitionSetup that describes the setting in which projections that shall
 * be reconstructed have been acquired.
 *
 * This takes a copy of \a setup to make it available during the time of reconstruction. Any
 * reconstruction performed after configure() was called, will use the passed \a setup to extract
 * geometry information about the scan. Note that \a setup must be consistent with the projections
 * that shall be reconstructed, that means it should contain information that actually corresponds
 * to the projections.
 *
 * Example:
 * \code
 *  // create a cylinder phantom
 *  const auto volume = VoxelVolume<float>::cylinderZ(50.0f, 100.0f, 1.0f, 0.02f);
 *
 *  // create an AcquisitionSetup for a flat panel short scan configuration
 *  AcquisitionSetup setup(makeSimpleCTSystem<blueprints::GenericCarmCT>(), 100);
 *  setup.applyPreparationProtocol(protocols::ShortScanTrajectory(700.0));
 *
 *  // simulate projections
 *  const auto projections = OCL::RayCasterProjector().configureAndProject(setup, volume);
 *
 *  // create the FDKReconstructor and configure the full 'setup'!
 *  OCL::FDKReconstructor rec;
 *  rec.configure(setup);
 *
 *  // do the actual reconstruction
 *  bool ok;
 *  auto recon = rec.reconstruct(projections, VoxelVolume<float>(volume.dimensions(),
 *                                                               volume.voxelSize()), &ok);
 *  qInfo() << ok;  // output: true
 *
 *  // visualize the result (note: requires 'gui_widgets.pri' submodule)
 *  gui::plot(recon);
 * \endcode
 *
 * Please also note that, in case that a subset of projections (by means of a ProjectionDataView)
 * is used in reconstruction later on, the \a setup passed here must still refer to the full
 * projection data set, the ProjectionDataView belongs to.
 *
 * Example:
 * \code
 *  // ... we continue the example from above
 *
 *  // we now want to select a subset of 'projections' that contains only every second view
 *  // -> create a vector [0, 2, 4, ... , 98]
 *  std::vector<uint> subsetIds = Range<uint>::linspace(0, 98, 50);
 *  // -> create the subset
 *  const auto projectionSubset = ProjectionDataView(projections, subsetIds);
 *
 *  // we can do the actual reconstruction without a new call to configure()
 *  // -> optional, but redundant: rec.configure(setup);
 *  recon = rec.reconstruct(projectionSubset, VoxelVolume<float>(volume.dimensions(),
 *                                                               volume.voxelSize()), &ok);
 *  qInfo() << ok;  // output: true
 *
 *  // visualize the result (note: requires 'gui_widgets.pri' submodule)
 *  gui::plot(recon);
 *
 *
 *  // if we would configure the setup for a subset...
 *  rec.configure(setup.subset(subsetIds));
 *  // ... and reconstruct the subset ...
 *  recon = rec.reconstruct(projectionSubset, VoxelVolume<float>(volume.dimensions(),
 *                                                               volume.voxelSize()), &ok);
 *  // ... we get the following error:
 *  // output during 'reconstruct':
 *  // "FDKReconstructor: Reconstruction aborted. Reason: projection data view contains a
 *  // view ID that exceeds the number of views in the configured setup."
 *
 *  qInfo() << ok;  // output: false
 *  \endcode
 */
void FDKReconstructor::configure(const AcquisitionSetup& setup)
{
    if(!setup.isValid())
        throw std::runtime_error("FDKReconstructor::configure: setup is invalid.");

    _setup = setup;
}

/*!
 * \brief Reconstructs projection data \a projections into \a targetVolume and returns \c true
 * on success.
 *
 * Note that the AcquisitionSetup corresponding to \a projections must have been set previously with
 * configure(). The requirements on the setup (i.e. acquisition geometry) are described in the
 * detailed class description and in isApplicableTo().
 *
 * \a targetVolume serves as an initialization, which means that data reconstructed from
 * \a projections will be added to any values already present in \a targetVolume. If \a targetVolume
 * has no memory allocated when passed here, memory allocation will be performed as part of the
 * backprojection step.
 *
 * Here is a break down of the individual processing steps:
 * 1. estimation of SDD and SID from the configured setup
 * 2. cosine weighting of \a projections (see also OCL::CosineWeighting)
 * 3. Parker weighting of \a projections with the chosen 'q' value (see setRevisedParkerWeightQ(),
 * more details can be found in OCL::ParkerWeightingRev)
 * 4. application of the projection filter (default: Ram--Lak-filter, see setProjectionFilter())
 * 5. backprojection of filtered projections (using OCL::SimpleBackprojector or
 * OCL::SFPBackprojector, see useSFPBackprojector() and useSimpleBackrojector())
 *
 * This instances notifier will broadcast status updates along the reconstruction process. You may
 * connect the notifier to the MessageHandler conveniently using connectNotifierToMessageHandler().
 *
 * Example:
 * \code
 *  // create a cube phantom
 *  const auto phantom = VoxelVolume<float>::cube(100, 1.0f, 0.02f);
 *
 *  // create an AcquisitionSetup for a flat panel short scan configuration (100 views)
 *  AcquisitionSetup setup(makeSimpleCTSystem<blueprints::GenericCarmCT>(), 100);
 *  setup.applyPreparationProtocol(protocols::ShortScanTrajectory(700.0));
 *
 *  // simulate projections
 *  const auto projections = OCL::RayCasterProjector().configureAndProject(setup, phantom);
 *
 *  // create the FDKReconstructor and configure the 'setup'!
 *  OCL::FDKReconstructor rec;
 *  rec.configure(setup);
 *
 *  // create the volume for reconstruction and reconstruct the data
 *  auto recVol = VoxelVolume<float>::cube(128, 1.0f, 0.0f); // note: we make it slightly larger than 'phantom'
 *  qInfo() << rec.reconstructToPlain(projections, recVol);  // output: true
 *
 *  // visualize the result (note: requires 'gui_widgets.pri' submodule)
 *  gui::plot(recVol);
 * \endcode
 */
bool FDKReconstructor::reconstructToPlain(const ProjectionDataView& projections,
                                          VoxelVolume<float>& targetVolume)
{
    qDebug() << "Start FDK reconstruction";

    // consistency checks
    if(!consistencyChecks(projections))
        return false;

    auto subsetSetup = _setup.subset(projections.viewIds());

    const auto SDD = estimateSDD(subsetSetup);
    const auto SID = estimateSID(subsetSetup);

    bool success{};
    auto filteredProj = projections.dataCopy();
    try {
        // ++++ Filter ++++
        // Cosine Weighting
        emit notifier()->progress(0.0);
        emit notifier()->information(QStringLiteral("Cosine weighting"));
        OCL::CosineWeighting cosine;
        cosine.filter(filteredProj, subsetSetup);
        emit notifier()->progress(0.2);

        // Parker
        emit notifier()->information(QStringLiteral("Parker weighting (rev.)"));
        OCL::ParkerWeightingRev parker(_q);
        parker.filter(filteredProj, subsetSetup);
        emit notifier()->progress(0.4);
        qDebug() << "Revised Parker weighting: q =" << _q;

        // Constant factor
        const float scaling = SID * SDD * parker.lastAngularRange() / (subsetSetup.nbViews() - 1);

        qDebug() << "SID [mm]:" << SID << "| SDD [pixel]:" << SDD
                 << "| angular range [deg]:" << parker.lastAngularRange() * 180.0 / PI;

        // Projection filter
        if(_projectionFilter)
        {
            emit notifier()->information(QStringLiteral("Filtering"));
            _projectionFilter->filter(filteredProj);
        }
        emit notifier()->progress(0.6);

        // ++++ Backprojection (with scaling factor) +++
        emit notifier()->information(QStringLiteral("Back projecting"));
        std::unique_ptr<BackprojectorBase> bp;
        const auto distWeighting = BackprojectorWeighting::DistanceWeightsOnly;
        if(_useSFPBackprojector)
            bp = makeReconstructor<OCL::SFPBackprojector>(OCL::SFPBackprojector::TR, distWeighting);
        else
            bp = makeReconstructor<OCL::SimpleBackprojector>(distWeighting);
        // set scaling factor
        bp->setCustomWeights( { projections.nbViews(), { scaling } } );
        // connect 'bp' to propagate progress message
        QObject::connect(bp->notifier(), &ReconstructorNotifier::progress, [this](qreal bpProgress) {
            emit notifier()->progress(0.6 + bpProgress * 0.4);
        });

        success = bp->configureAndReconstructTo(subsetSetup, filteredProj, targetVolume);

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";
    } catch(const std::bad_alloc& except)
    {
        qCritical() << "Allocation error:" << except.what();
    } catch(const std::exception& except)
    {
        qCritical() << "std exception:" << except.what();
    }

    return success;
}

/*!
 * \brief Constructs an FDKReconstructor with revised Parker weighting \a q; enables use of the
 * separable footprint backprojector if \a useSFPBackprojector = \c true.
 */
FDKReconstructor::FDKReconstructor(float q,
                                   bool useSFPBackprojector)
    : FDKReconstructor(new OCL::RamLakFilter, q, useSFPBackprojector)
{
}

/*!
 * \brief Constructs an FDKReconstructor with a custom projection filter \a projectionFilter,
 * revised Parker weighting \a q, and use of the separable footprint backprojector if
 * \a useSFPBackprojector = \c true.
 *
 * See setProjectionFilter() for details on the projection filter.
 */
FDKReconstructor::FDKReconstructor(std::unique_ptr<ProjectionFilter> projectionFilter,
                                   float q,
                                   bool useSFPBackprojector)
    : _projectionFilter(std::move(projectionFilter))
    , _q(q)
    , _useSFPBackprojector(useSFPBackprojector)
{
}

/*!
 * \brief Constructs an FDKReconstructor with a custom projection filter \a projectionFilter,
 * revised Parker weighting \a q, and use of the separable footprint backprojector if
 * \a useSFPBackprojector = \c true.
 *
 * See setProjectionFilter() for details on the projection filter.
 */
FDKReconstructor::FDKReconstructor(ProjectionFilter* projectionFilter,
                                   float q,
                                   bool useSFPBackprojector)
    : _projectionFilter(projectionFilter)
    , _q(q)
    , _useSFPBackprojector(useSFPBackprojector)
{
}

/*!
 * \brief Sets the parameter 'q' for the revised Parker weighting to \a q.
 *
 * The 'q' parameter controls the smoothness between areas of redundant data. This can positively
 * effect noise reduction, especially in case of considerable overscan.
 *
 * For values \f$ 0 < q < 1 \f$, the signal-to-noise ratio can be improved. However, too small
 * values can lead to high frequency artifacts, in particular in case of a low number of views.
 * For \f$ q=1 \f$, the weighting coincides with the classical Parker weights. Setting \f$ q=0 \f$
 * is recommended only in case of full scan data (i.e. full 360 degree scan orbit). In that case,
 * weights for all views will be 0.5.
 *
 * \sa OCL::ParkerWeightingRev
 */
void FDKReconstructor::setRevisedParkerWeightQ(float q) { _q = q; }

/*!
 * \brief Enables use of the OCL::SFPBackprojector for the backprojection operation.
 *
 * To switch back to the OCL::SimpleBackprojector, call useSimpleBackrojector().
 *
 * Example:
 * \code
 * OCL::FDKReconstructor rec;
 * qInfo() << rec.isSFPBackprojectorInUse(); // output: false [default is simple backprojector]
 *
 * rec.useSFPBackprojector();
 * qInfo() << rec.isSFPBackprojectorInUse(); // output: true
 * \endcode
 */
void FDKReconstructor::useSFPBackprojector() { _useSFPBackprojector = true; }

/*!
 * \brief Enables use of the OCL::SimpleBackprojector for the backprojection operation.
 *
 * To enable use of the OCL::SFPBackprojector, call useSFPBackprojector().
 *
 * Example:
 * \code
 * // create an FDKReconstructor that uses the SFPBackprojector (2nd parameter: 'true')
 * OCL::FDKReconstructor rec(1.0f, true);
 * qInfo() << rec.isSimpleBackprojectorInUse(); // output: false
 *
 * // switch back to the simple backprojector
 * rec.useSimpleBackrojector();
 * qInfo() << rec.isSimpleBackprojectorInUse(); // output: true
 * \endcode
 */
void FDKReconstructor::useSimpleBackrojector() { _useSFPBackprojector = false; }

/*!
 * \brief Sets the projection filter to \a projectionFilter.
 *
 * The projection filter is applied to input projections before backprojecting; corresponds to step
 * 4 in the workflow (see reconstructToPlain()).
 *
 * By default, OCL::RamLakFilter will be used for filtering.
 *
 * Example: use an ApodizationFilter with filter type 'Cosine' and a frequency scaling of 0.5
 * \code
 *  OCL::FDKReconstructor rec;
 *  auto filter = std::unique_ptr<OCL::ApodizationFilter>(new OCL::ApodizationFilter(OCL::ApodizationFilter::Cosine, 0.5f));
 *  rec.setProjectionFilter(std::move(filter));
 * \endcode
 */
void FDKReconstructor::setProjectionFilter(std::unique_ptr<ProjectionFilter> projectionFilter)
{
    _projectionFilter.swap(projectionFilter);
}

/*!
 * \brief Sets the projection filter to \a projectionFilter.
 *
 * The projection filter is applied to input projections before backprojecting; corresponds to step
 * 4 in the workflow (see reconstructToPlain()).
 *
 * By default, OCL::RamLakFilter will be used for filtering.
 *
 * Example: use an ApodizationFilter with filter type 'Cosine' and a frequency scaling of 0.5
 * \code
 *  OCL::FDKReconstructor rec;
 *  rec.setProjectionFilter(new OCL::ApodizationFilter(OCL::ApodizationFilter::Cosine, 0.5f));
 * \endcode
 */
void FDKReconstructor::setProjectionFilter(ProjectionFilter* projectionFilter)
{
    _projectionFilter.reset(projectionFilter);
}

/*!
 * \brief Returns \c true if OCL::SFPBackprojector is in use for the backprojection operation.
 *
 * By default, the OCL::SimpleBackprojector is used. See also useSFPBackprojector().
 *
 * Example:
 * \code
 * OCL::FDKReconstructor rec;
 * qInfo() << rec.isSFPBackprojectorInUse(); // output: false [default is simple backprojector]
 *
 * rec.useSFPBackprojector();
 * qInfo() << rec.isSFPBackprojectorInUse(); // output: true
 * \endcode
 */
bool FDKReconstructor::isSFPBackprojectorInUse() const { return _useSFPBackprojector; }

/*!
 * \brief Returns \c true if OCL::SimpleBackprojector is in use for the backprojection operation.
 *
 * Example:
 * \code
 * OCL::FDKReconstructor rec;
 * qInfo() << rec.isSimpleBackprojectorInUse(); // output: true [default is simple backprojector]
 * \endcode
 */
bool FDKReconstructor::isSimpleBackprojectorInUse() const { return !_useSFPBackprojector; }

/*!
 * \brief Returns the current 'q' value used in revised Parker weighting.
 *
 * See also setRevisedParkerWeightQ().
 */
float FDKReconstructor::revisedParkerWeightQ() const { return _q; }

/*!
 * \copybrief AbstractReconstructor::isApplicableTo
 *
 * This verifies whether or not \a setup fulfills the requirements that FDK reconstruction can be
 * applied to projection data from a corresponding acquisition.
 *
 * In particular it checks if:
 * - projection data has only a single module (flat panel projections) (*)
 * - projection data has at least 3 (different) views (*)
 * - the scan trajectory is a circular orbit or at least a segment thereof (i.e. 'short scan'
 * covering 180° plus fan angle)
 * - the detector rows lay tangential on the scan trajectory
 *
 * This returns \c false if any of the requirements are not met. However, they differ in severity
 * as detailed below:
 *
 * - Requirements marked with (*) are hard requirements. Reconstruction attempts on data that
 * violate one (or both) of them are aborted. [violation is reported as a qCritical]
 * - If any of the other requirements is (partially or fully) violated, reconstruction can still be
 * carried out, but the result may be improper. [violation is reported as a qWarning]
 */
bool FDKReconstructor::isApplicableTo(const AcquisitionSetup& setup) const
{
    if(setup.nbViews() < 3)
    {
        qCritical("FDKReconstructor requires at least three views.");
        return false;
    }
    if(setup.system()->detector()->nbDetectorModules() != 1u)
    {
        qCritical() << "FDKReconstructor requires detector with a single module.";
        return false;
    }

    AcquisitionSetup setupCpy(setup);

    return isCircularSourceOrbit(setupCpy) && isDetectorTangentialToOrbit(setupCpy);
}

/*!
 * \copybrief AbstractReconstructor::parameter
 *
 * Returns a QVariantMap including the following (key, value)-pairs:
 *
 * - ("parker q", [float] Parker weighting q value),
 * - ("use sfp backprojector", [bool] whether or not footprint backprojector is used),
 * - ("projection filter", [QVariant] full QVariant of the projection filter).
 */
QVariant FDKReconstructor::parameter() const
{
    QVariantMap ret = AbstractReconstructor::parameter().toMap();

    ret.insert(QStringLiteral("parker q"), _q);
    ret.insert(QStringLiteral("use sfp backprojector"), _useSFPBackprojector);
    ret.insert(QStringLiteral("projection filter"), _projectionFilter->toVariant());

    return ret;
}

/*!
 * \copybrief AbstractReconstructor::setParameter
 *
 * The passed \a parameter must be a QVariantMap containing one or more of the following
 * (key, value)-pairs:
 *
 * - ("parker q", [float] Parker weighting q value),
 * - ("use sfp backprojector", [bool] whether or not footprint backprojector is used),
 * - ("projection filter", [QVariant] full QVariant of the projection filter).
 *
 * Note that it is strongly discouraged to use this method to set individual parameters of the
 * instance. Please consider using individual setter methods for that purpose.
 */
void FDKReconstructor::setParameter(const QVariant& parameter)
{
    AbstractReconstructor::setParameter(parameter);

    const auto parMap = parameter.toMap();

    if(parMap.contains(QStringLiteral("parker q")))
        setRevisedParkerWeightQ(parMap.value(QStringLiteral("parker q")).toFloat());
    if(parMap.contains(QStringLiteral("use sfp backprojector")))
        parMap.value(QStringLiteral("use sfp backprojector")).toBool()
                ? useSFPBackprojector()
                : useSimpleBackrojector();
    if(parMap.contains(QStringLiteral("projection filter")))
    {
        const auto filterVariant = parMap.value(QStringLiteral("projection filter"));
        auto parsedFilterObj = SerializationHelper::parseMiscObject(filterVariant);

        if(!dynamic_cast<GenericOCLProjectionFilter*>(parsedFilterObj))
        {
            delete parsedFilterObj;
            throw std::runtime_error("FDKReconstructor::setParameter(): "
                                     "Could not successfully parse projection filter object.");
        }

        setProjectionFilter(static_cast<GenericOCLProjectionFilter*>(parsedFilterObj));
    }
}

/*!
 * \brief Estimates the source-to-detector distance used in \a setup.
 *
 * This estimation is based on the system configuration in the first view contained in \a setup.
 * Note that acquisition setups with varying SDD are not considered correctly.
 */
float FDKReconstructor::estimateSDD(AcquisitionSetup& setup)
{
    GeometryEncoder enc(setup.system());
    setup.prepareView(0);

    return (enc.finalSourcePosition() - enc.finalModulePosition(0)).norm()
            / setup.system()->detector()->pixelDimensions().width();
}

/*!
 * \brief Estimates the source-to-isocenter distance used in \a setup.
 *
 * This estimation is based on a fit of a circular orbit based on the source positions of three
 * views from \a setup. For more details, see assist::circleFromSetup().

 */
float FDKReconstructor::estimateSID(AcquisitionSetup& setup)
{
    return circleFromSetup(setup).radius;
}

/*!
 * \brief Returns \c true if the source positions in the views defined by \a setup can be considered
 * to lie on a circular orbit.
 *
 * This first estimates the circular orbit formed by the source positions in \a setup and then
 * checks the distance of the individual source positions (i.e. for each view) from the estimated
 * orbit. If a position exceeds a distance of 10 mm from the orbit, it is considered as an invalid
 * view and the result of this method call will be \c false. A warning message will be communicated containing the total number of invalid views. Information on the individual view level (i.e.
 * id of the invalid view and the actual distance to the orbit) is communicated as a debug message.
 * If all views' source positions fall within the tolerance level (10 mm) w.r.t. the estimated
 * circular orbit, this returns \c true.
 */
bool FDKReconstructor::isCircularSourceOrbit(AcquisitionSetup& setup)
{
    const auto nbViews = setup.nbViews();

    const auto circle = circleFromSetup(setup);

    // check distances of all other source positions from that circle
    static constexpr auto DIST_TOL = 10.0;
    uint nbInvalViews = 0;
    for(uint v = 0; v < nbViews; ++v)
    {
        setup.prepareView(v);
        const auto srcPos = setup.system()->gantry()->sourcePosition();
        const auto dist = circle.distanceToPoint(srcPos);
        if(dist > DIST_TOL)
        {
            ++nbInvalViews;
            qDebug() <<"View" << v << "source position distance from circular orbit:" << dist;
        }
    }

    if(nbInvalViews)
    {
        qWarning() << "FDKReconstructor: Non-circular orbit detected.\n"
                   << nbInvalViews << "views have a source position that is more than"
                   << DIST_TOL << "mm away from the estimated circular orbit.";

        return false;
    }

    return true;
}

/*!
 * \brief Returns \c true if the detector orientation for all the views defined by \a setup is such
 * that its rows lay tangential on the scan trajectory.
 *
 * This first estimates the circular orbit formed by the source positions in \a setup (see
 * assist::circleFromSetup) and then checks the angulation between the first axis of the detector
 * (row dimension) and the circular trajectory for each individual view. If an angulation exceeds
 * the threshold of 1 degree, it is considered as an invalid view and the result of this method
 * call will be \c false. A warning message will be communicated containing the total number of
 * invalid views. Information on the individual view level (i.e. id of the invalid view and the
 * actual angle of the detector with the orbit) is communicated as a debug message.
 * If all views' detector orientations fall within the tolerance level (1 degree) w.r.t. the
 * estimated circular orbit, this returns \c true.
 */
bool FDKReconstructor::isDetectorTangentialToOrbit(AcquisitionSetup& setup)
{
    const auto ANGLE_TOL = std::sin(static_cast<double>(1.0_deg)); // = cos(90° - 1°)

    // get trajectory circle
    const auto circle = circleFromSetup(setup);

    // check detector orientation for each view
    uint nbInvalViews = 0;
    GeometryEncoder enc(setup.system());
    for(uint v = 0, nbViews = setup.nbViews(); v < nbViews; ++v)
    {
        setup.prepareView(v);
        const auto detAxisU = enc.finalModuleRotation(0).row<0>();
        const auto cosAngle = detAxisU * circle.normal;
        if(std::abs(cosAngle) > ANGLE_TOL)
        {
            ++nbInvalViews;
            qDebug() << QStringLiteral("View %1: detector u-axis not tangential on circular orbit"
                                       " (angle: %2).").arg(v)
                                                       .arg(std::asin(cosAngle) * 180.0 / PI);
        }
    }

    if(nbInvalViews)
    {
        qWarning() << QStringLiteral("FDKReconstructor: Detector orientation detected that is not"
                                     " tangential on scan orbit. %1 views have an angle of more"
                                     " than %2 degree(s)").arg(nbInvalViews)
                                                          .arg(std::asin(ANGLE_TOL) * 180.0 / PI);

        return false;
    }

    return true;
}

/*!
 * \brief Returns \c true if \a projections are consistent with the AcquisitionSetup previously
 * passed to configure().
 *
 * In particular, this validates whether:
 * - The configured setup is valid (see AcquisitionSetup::isValid()),
 * - the number of modules in \a projections is one (this class cannot handle multi-module data),
 * - \a projections contains no view id larger than the number of views in the configured setup,
 * - \a projections contains at least three different views (w.r.t their view ids).
 *
 * Returns \c true is all abovementioned criteria are satisfied.
 * Returns \c false if any of them is not fulfilled; note that criteria are checked in the order
 * they are listed above. Issues a warning message if a criterion is not fulfilled and returns
 * immediately afterwards, i.e. criteria appearing later in the list will not be checked.
 */
bool FDKReconstructor::consistencyChecks(const ProjectionDataView& projections) const
{
    const auto nbViewsInSetup = _setup.nbViews();
    const auto nbModules = projections.viewDimensions().nbModules;
    const auto& viewIds = projections.viewIds();

    if(!_setup.isValid())
    {
        qCritical() << "FDKReconstructor: Reconstruction aborted. "
                       "Reason: invalid AcquisitionSetup. Did you forget to call configure()?";
        return false;
    }
    if(nbModules != 1u)
    {
        qCritical() << "FDKReconstructor: Reconstruction aborted. "
                       "Reason: projection data view contains data more than one module.";
        return false;
    }
    if(std::any_of(viewIds.cbegin(), viewIds.cend(),
                   [nbViewsInSetup] (uint viewID) { return viewID >= nbViewsInSetup; } ))
    {
        qCritical() << "FDKReconstructor: Reconstruction aborted. "
                       "Reason: projection data view contains a view ID that exceeds the number of "
                       "views in the configured setup.";
        return false;
    }
    if(std::set<uint>(viewIds.cbegin(), viewIds.cend()).size() < 3)
    {
        qCritical() << "FDKReconstructor: Reconstruction aborted. "
                       "Reason: requires at least three (different) views.";
        return false;
    }

    return true;
}

/*!
 * \brief Estimates a circular orbit from three source positions defined by \a setup.
 * \relates FDKReconstructor
 *
 * This uses the source positions of the system, prepared for three particular views, to estimate a
 * circle on which the source positions are located. The three used views are:
 * - 0 [first view]
 * - 1/3 * nbViews
 * - 2/3 * nbViews,
 * where nbViews is the total number of views in \a setup (`setup.nbViews()`).
 *
 * This requires at least three views in \a setup; throws `std::domain_error` otherwise.
 */
static assist::Circle3D circleFromSetup(AcquisitionSetup& setup)
{
    const auto nbViews = setup.nbViews();
    if(nbViews < 3)
        throw std::domain_error("Circle3D::circleFromSetup: "
                                "estimation requires at least three views.");

    // select three view positions
    const auto viewID0 = 0u;
    const auto viewID1 = std::max(    nbViews / 3, viewID0 + 1);
    const auto viewID2 = std::max(2 * nbViews / 3, viewID1 + 1);

    // get view positions
    setup.prepareView(viewID0);
    const auto c0 = setup.system()->gantry()->sourcePosition();
    setup.prepareView(viewID1);
    const auto c1 = setup.system()->gantry()->sourcePosition();
    setup.prepareView(viewID2);
    const auto c2 = setup.system()->gantry()->sourcePosition();

    // compute circle orbit for c0, c1, c2
    return assist::Circle3D::fromThreePoints(c0, c1, c2);
}

} // namespace OCL

namespace assist {

/*!
 * \brief Returns the (shortest) Euclidean distance between the point \a pt and the circle.
 */
double Circle3D::distanceToPoint(const mat::Matrix<3, 1>& pt) const
{
    const auto delta = pt - center;
    return std::sqrt(std::pow(dot(normal, delta), 2.0)
                   + std::pow(cross(normal, delta).norm() - radius, 2.0));
}

/*!
 * \brief Constructs a Circle3D from the three points \a p1, \a p2, and \a p3.
 *
 * The three input points must not all lie on a straight line. Issues a warning message otherwise
 * and returns an undefined result.
 */
Circle3D Circle3D::fromThreePoints(const mat::Matrix<3, 1>& p1,
                                   const mat::Matrix<3, 1>& p2,
                                   const mat::Matrix<3, 1>& p3)
{
    // ### explicit solution ####

    // connection vectors
    const auto BA = p2 - p1;
    const auto CA = p3 - p1;

    // u, v: (unit-)vectors spanning the plane of the circle
    // w: normal of the plane that contains the circle
    const auto u = BA.normalized();
    const auto w = cross(CA, u).normalized();
    const auto v = cross(w, u);

    // check for 'nan' in w; occurs when all points in a line (hence, BA || CA) -> no circle possible
    if(std::any_of(w.constBegin(), w.constEnd(),[](const double& val){ return std::isnan(val); } ))
        qWarning() << "Circle3D::fromThreePoints: Points do not form a proper circle.";

    // 2D projection
    const auto bx_2 = 0.5 * dot(BA, u);
    const auto cx = dot(CA, u);
    const auto cy = dot(CA, v);

    const auto h = (std::pow(cx - bx_2, 2.0) + std::pow(cy, 2.0) - std::pow(bx_2, 2.0)) / (2.0*cy);
    const auto circleCenter = p1 + bx_2*u + h*v;

    const auto r = (  (p1 - circleCenter).norm()
                    + (p2 - circleCenter).norm()
                    + (p3 - circleCenter).norm() ) / 3.0;

    return Circle3D{ circleCenter, w.normalized(), r };


    /*
    // ### intersection of planes (lin. equation system) ####

    // vectors spanning the plane of the circle
    const auto n1 = p2 - p1;
    const auto n2 = p3 - p1;
    // normal of the plane that contains the circle
    const auto n3 = cross(n1, n2);
    // distances of planes that intersect the circle center
    const auto d1 = dot(n1, (p1 + 0.5 * n1));
    const auto d2 = dot(n2, (p1 + 0.5 * n2));
    const auto d3 = dot(n3, p1);

    // intersection of the three planes
    const auto A = horzcat(horzcat(n1, n2), n3).transposed();
    const auto b = Vector3x1{ d1, d2, d3 };
    const auto circleCenter = linSolve(A, b);

    const auto r = (  (p1 - circleCenter).norm()
                    + (p2 - circleCenter).norm()
                    + (p3 - circleCenter).norm() ) / 3.0;

    return Circle3D{ circleCenter, n3.normalized(), r };
    */
}

} // namespace assist

} // namespace CTL
