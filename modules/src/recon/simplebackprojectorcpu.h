#ifndef CTL_SIMPLEBACKPROJECTORCPU_H
#define CTL_SIMPLEBACKPROJECTORCPU_H

#include "abstractreconstructor.h"
#include "acquisition/acquisitionsetup.h"

#include <QObject>

namespace CTL {

class FullGeometry;

namespace mat {
template<uint, uint> class Matrix;
class ProjectionMatrix;
}

/*!
 * \class SimpleBackprojectorCPU
 * \brief The SimpleBackprojectorCPU class is a CPU implementation of a voxel-driven backprojector
 * using the "back-targeting" approach.
 *
 * This class implements a voxel-driven backprojection approach that uses a projection operation to
 * identify which projection data needs to be backprojected to a certain voxel. In brief, the
 * routine works as follows: For each voxel V (with its x, y, z coordinates) in the target volume,
 * and given the projection matrix P of a particular view (and module), we compute the location
 * where V gets projected (using homogeneous coordinates and the corresponding projection matrix
 * product) and read-out the corresponding value from the projection data (zero if outside). This
 * value is weighted according to the chosen weighting type (see setWeightingType() for more
 * details) and the result is added to the current value of V. This procedure is carried out for all
 * views (and modules) of the projection data.
 *
 * For more details on usage of reconstructors in general, please also refer to the documentation
 * of AbstractReconstructor.
 *
 * Example:
 * \code
 *  // create a cube phantom
 *  const auto phantom = VoxelVolume<float>::cube(100, 1.0f, 0.02f);
 *
 *  // create an AcquisitionSetup for a flat panel short scan configuration (100 views)
 *  AcquisitionSetup setup(makeSimpleCTSystem<blueprints::GenericCarmCT>(), 100);
 *  setup.applyPreparationProtocol(protocols::ShortScanTrajectory(700.0));
 *
 *  // simulate projections
 *  const auto projections = OCL::RayCasterProjector().configureAndProject(setup, phantom);
 *
 *  // create the SimpleBackprojectorCPU
 *  SimpleBackprojectorCPU rec;
 *
 *  // configure the acquisition geometry and reconstruct the data
 *  // note: we reconstruct a volume slightly larger than 'phantom'
 *  bool ok;
 *  const auto recVol = rec.configureAndReconstruct(setup, projections,
 *                                                  VoxelVolume<float>::cube(128, 1.0f, 0.0f), &ok);
 *  qInfo() << ok; // output: true
 *
 *  // visualize the result (note: requires 'gui_widgets.pri' submodule)
 *  gui::plot(recVol);
 * \endcode
 */
class SimpleBackprojectorCPU : public AbstractReconstructor
{
    CTL_TYPE_ID(2)
    Q_GADGET

    public: void configure(const AcquisitionSetup& setup) override;

public:   
    enum WeightingType { DistanceWeightsOnly, GeometryFactors, CustomWeights };
    Q_ENUM(WeightingType)

    explicit SimpleBackprojectorCPU(WeightingType weightType = GeometryFactors);

    // SerializationInterface interface
    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;
    QVariant toVariant() const override;

    // AbstractReconstructor interface
    bool reconstructToPlain(const ProjectionDataView& projections, VoxelVolume<float>& targetVolume) override;

    // setters
    void setInterpolate(bool interpolate);
    void setCustomWeights(std::vector<float> weights);
    void setWeightingType(WeightingType weightType);

private:
    AcquisitionSetup _setup;
    std::vector<float> _customWeights;
    std::vector<float> _weights;
    bool _interpolate = false;
    WeightingType _weighting;

    void processModule(const Chunk2D<float>& moduleData,
                       const mat::ProjectionMatrix& P1,
                       VoxelVolume<float>& targetVolume,
                       float moduleWeight);
    bool selectModuleWeights(const FullGeometry& pMats, const VoxelVolume<float>& targetVolume);

    static std::vector<float> geoWeights(const FullGeometry& pMats, const VoxelVolume<float>& targetVolume);
    static QMetaEnum metaEnum();
};

namespace assist {
    float interpolate2D(const Chunk2D<float>& moduleData, const mat::Matrix<2,1>& pos);
} // namespace assist

} // namespace CTL

#endif // CTL_SIMPLEBACKPROJECTORCPU_H
