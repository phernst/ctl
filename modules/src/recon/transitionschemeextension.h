#ifndef CTL_TRANSITIONSCHEMEEXTENSION_H
#define CTL_TRANSITIONSCHEMEEXTENSION_H

#include "abstractsubsetgenerator.h"
#include <memory>

namespace CTL {

/*!
 * \brief The TransitionSchemeExtension class is a decorator for subset generators enabling a
 * transition from many small subsets to a few larger ones with increasing iteration.
 *
 * This class extends the capabilities of a subset generator (henceforth also referred to as the
 * nested generator) by taking control of the number of subsets the nested generator creates for a
 * particular iteration. In doing so, this extension provides a means to generate subsets of
 * increasing size for consecutive iterations. To be more specific, subset generation starts with a
 * given upper bound for the number of subsets (see setMaximumNbSubsets()) and uses the nested
 * generator to generate that particular amount of subsets for the first iteration. After a defined
 * number of iterations (see setTransitionPeriod()), the number of subsets will be halved, resulting
 * in fewer but larger subsets for the upcoming iteration/s. This procedure of cutting the number of
 * subsets in half is repeated until a defined lower bound of subsets (see setMinimumNbSubsets()) is
 * reached.
 * For convenience, initial and final subset sizes can also be be specified in terms of bounds (see
 * setSubsetsBounds()), offering a combined setter for the number of subsets to be created in the
 * two opposing limits (i.e. largest and smallest number of subsets).
 *
 * \copydetails TransitionSchemeExtension::numberOfSubsets
 *
 * To be functional, this extension needs an actual subset generator to be set (during construction;
 * exchangeable later on through setSubsetGenerator()). Depending on the requirements of the nested
 * generator, the extension does or does not need the AcquisitionSetup that corresponds to the
 * projections that shall be partitioned (see setSetup()). Please refer to the documentation of the
 * specific generator for details on that aspect.
 *
 * Example:
 * \code
 *  // create dummy projection data (sizes entirely arbitrary, and irrelevant here)
 *  ProjectionData dummy(1,1,1);
 *  dummy.allocateMemory(40);   // 'dummy' now contains 40 views
 *
 *  // create the nested generator: a SimpleSubsetGenerator with generation pattern 'Adjacent'
 *  auto nestedGen = new SimpleSubsetGenerator;
 *  nestedGen->setOrder(SimpleSubsetGenerator::Adjacent);
 *  // NOTE: we could set a number of subsets to 'nestedGen', but it would have no effect anyways
 *  // nestedGen->setNbSubsets(3);  // if you uncomment this line, nothing changes in the results
 *
 *  // create our TransitionSchemeExtension 'on top' of the nested generator
 *  // NOTE: 'subsetGen' takes ownership of 'nestedGen'
 *  TransitionSchemeExtension subsetGen(nestedGen);
 *  // here, we specify our desired number of subsets: starting with 4 subsets and finishing with 1
 *  subsetGen.setSubsetBounds(4,1);
 *
 *  // pass the dummy projection data
 *  // NOTE: the nested generator (SimpleSubsetGenerator) does not need an AcquisitionSetup to be set
 *  subsetGen.setProjections(dummy);
 *
 *  // generate (and inspect) subsets for the first 4 iterations
 *  for(uint it = 0; it < 4; ++it)
 *  {
 *      const auto subsets = subsetGen.generateSubsets(it);
 *
 *      qInfo() << "Iteration" << it << "with" << subsets.size() << "subset(s)";
 *
 *      // print the ids of the views included in the two subsets
 *      for(const auto& subset : subsets)
 *          qInfo() << subset.viewIds();
 *  }
 *
 *  // output:
 *  // Iteration 0 with 4 subset(s)
 *  // std::vector(20, 21, 22, 23, 24, 25, 26, 27, 28, 29)
 *  // std::vector(30, 31, 32, 33, 34, 35, 36, 37, 38, 39)
 *  // std::vector(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
 *  // std::vector(10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
 *
 *  // Iteration 1 with 2 subset(s)
 *  // std::vector(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
 *  // std::vector(20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39)
 *
 *  // Iteration 2 with 1 subset(s)
 *  // std::vector(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39)
 *
 *  // Iteration 3 with 1 subset(s)
 *  // std::vector(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39)
 *
 * \endcode
 */
class TransitionSchemeExtension : public AbstractSubsetGenerator
{
    CTL_TYPE_ID(1100)

    protected: std::vector<ProjectionDataView> generateSubsetsImpl(uint iteration) const override;

public:
    explicit TransitionSchemeExtension(AbstractFixedSizeSubsetGenerator* nestedGenerator);
    explicit TransitionSchemeExtension(std::unique_ptr<AbstractFixedSizeSubsetGenerator> nestedGenerator);

    virtual void setSetup(const AcquisitionSetup& setup) override;
    virtual void setProjections(ProjectionDataView projections) override;

    // SerializationInterface interface
    void fromVariant(const QVariant& variant) override;
    QVariant toVariant() const override;

    void setTransitionPeriod(uint transitionEveryNIterations);
    void setMaximumNbSubsets(uint maxNbSubsets);
    void setMinimumNbSubsets(uint minNbSubsets);
    void setSubsetBounds(uint initialNbSubsets, uint finalNbSubsets);

    void setSubsetGenerator(AbstractFixedSizeSubsetGenerator* nestedGenerator);
    void setSubsetGenerator(std::unique_ptr<AbstractFixedSizeSubsetGenerator> nestedGenerator);

protected:
    TransitionSchemeExtension() = default;

    std::unique_ptr<AbstractFixedSizeSubsetGenerator> _nestedGen;

    uint _minNbSubsets = 1u;
    uint _maxNbSubsets = 0u;
    uint _transitionPeriod = 1u;

    virtual uint numberOfSubsets(uint iteration) const;
    void shuffleSubsets(std::vector<ProjectionDataView> &subsets) const override;
};

} // namespace CTL

#endif // CTL_TRANSITIONSCHEMEEXTENSION_H
