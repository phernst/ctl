#ifndef CTL_DEFAULTSUBSETGENERATOR_H
#define CTL_DEFAULTSUBSETGENERATOR_H

#include "abstractsubsetgenerator.h"
#include "transitionschemeextension.h"

#include <QObject>

namespace CTL {

/*!
 * \brief The DefaultSubsetGenerator class is a convenience class combining the capabilities of
 * multiple CTL subset generators.
 *
 * The DefaultSubsetGenerator provides the capabilities of a TransitionSchemeExtension with
 * predefined nested subset generators to make its use more convenient. This includes the
 * SimpleSubsetGenerator as well as the OrthogonalSubsetGenerator. The corresponding generator type
 * will be selected automatically based on the chosen subset order (or generation pattern) by means
 * of setOrder().
 *
 * By default, this class creates a subset size transition with progressing iteration count. Refer
 * to the documentation of TransitionSchemeExtension for more details on all possible settings and
 * and explanation of the transition pattern. In case a fixed number of subsets is desired, this can
 * be done conveniently using setFixedNumberOfSubsets().
 *
 * In total, this generator supports the following generation patterns:
 * - Random         - randomly selects views for a subset
 * - Adjacent       - puts adjacent views into a subset
 * - Orthogonal180  - selects sets of most orthogonal views, assuming the total scan range was 180 degrees
 * - Orthogonal360  - selects sets of most orthogonal views, assuming the total scan range was 360 degrees
 * - RealOrthogonal - selection based on orthogonality between real view directions (i.e. the source-to-detector vector)
 *
 * Note that when using Order::RealOrthogonal, subset generation depends on the actual acquisition
 * geometry, thus the corresponding AcquisitionSetup needs to be set in advance of generating the
 * subsets (see setSetup()).
 *
 * It is also possible to use TransitionSchemeExtension::setSubsetGenerator() to install a custom
 * nested subset generator. However, a call to setOrder() will replace any previously installed
 * custom generator by either SimpleSubsetGenerator or OrthogonalSubsetGenerator (depending on the
 * selected order).
 *
 * Example:
 * \code
 *  constexpr uint nbViews = 18;
 *
 *  // first, we create the AcquisitionSetup with 'nbViews' and a ShortScanTrajectory
 *  AcquisitionSetup setup(makeSimpleCTSystem<blueprints::GenericCarmCT>());
 *  setup.setNbViews(nbViews);
 *  setup.applyPreparationProtocol(protocols::ShortScanTrajectory(700.0));
 *
 *  // create some dummy projections (the actual dimensions are irrelevant here)
 *  ProjectionData dummyProjections(1,1,1);
 *  dummyProjections.allocateMemory(nbViews); // dummyProjections now contains 'nbViews' views
 *
 *  // create our subset generator
 *  DefaultSubsetGenerator subsetGen;
 *
 *  // we choose the 'Orthogonal180' pattern and want to transition between 6 and 2 subsets:
 *  subsetGen.setOrder(DefaultSubsetGenerator::Orthogonal180);
 *  subsetGen.setSubsetBounds(6, 2);
 *
 *  // pass projection data to the generator
 *  subsetGen.setProjections(dummyProjections);
 *  // NOTE: except for DefaultSubsetGenerator::RealOrthogonal, we don't need to pass the setup
 *  // However, it would also be ok to do so, without additional cost:
 *  // subsetGen.setData(dummyProjections, setup);
 *
 *  // generate (and inspect) subsets for the first 4 iterations
 *  for(uint it = 0; it < 4; ++it)
 *  {
 *      const auto subsets = subsetGen.generateSubsets(it);
 *
 *      qInfo() << "Iteration" << it << "with" << subsets.size() << "subset(s)";
 *
 *      // print the ids of the views included in the two subsets
 *      for(const auto& subset : subsets)
 *          qInfo() << subset.viewIds();
 *  }
 *
 *  // output:
 *  // Iteration 0 with 6 subset(s)
 *  // std::vector(8, 17, 5)
 *  // std::vector(2, 11, 3)
 *  // std::vector(0, 9, 4)
 *  // std::vector(14, 6, 15)
 *  // std::vector(13, 7, 16)
 *  // std::vector(12, 1, 10)
 *
 *  // Iteration 1 with 3 subset(s)
 *  // std::vector(0, 9, 4, 13, 7, 16)
 *  // std::vector(2, 11, 3, 12, 1, 10)
 *  // std::vector(8, 17, 5, 14, 6, 15)
 *
 *  // Iteration 2 with 2 subset(s)
 *  // std::vector(12, 1, 10, 8, 17, 5, 14, 6, 15)
 *  // std::vector(0, 9, 4, 13, 7, 16, 2, 11, 3)
 *
 *  // Iteration 3 with 2 subset(s)
 *  // std::vector(0, 9, 4, 13, 7, 16, 2, 11, 3)
 *  // std::vector(12, 1, 10, 8, 17, 5, 14, 6, 15)
 * \endcode
 *
 *  We now want to use the RealOrthogonal pattern to generate our subsets. We can do so as follows:
 * \code
 *  subsetGen.setOrder(DefaultSubsetGenerator::RealOrthogonal);
 *
 *  // since this generation pattern relies on the real acquisition geometry,
 *  // we need to also pass 'setup' to 'subsetGen', this time.
 *  subsetGen.setData(dummyProjections, setup);
 *  // NOTE: we need to pass the projections again after changing the order!
 *
 *  // we repeat the generation of subsets for the first 4 iterations
 *  for(uint it = 0; it < 4; ++it)
 *  {
 *      const auto subsets = subsetGen.generateSubsets(it);
 *
 *      qInfo() << "Iteration" << it << "with" << subsets.size() << "subset(s)";
 *
 *      // print the ids of the views included in the two subsets
 *      for(const auto& subset : subsets)
 *          qInfo() << subset.viewIds();
 *  }
 *
 *  // output:
 *  // Iteration 0 with 6 subset(s)
 *  // std::vector(15, 2, 1)
 *  // std::vector(0, 8, 4)
 *  // std::vector(14, 6, 10)
 *  // std::vector(17, 9, 13)
 *  // std::vector(16, 5, 12)
 *  // std::vector(11, 3, 7)
 *
 *  // Iteration 1 with 3 subset(s)
 *  // std::vector(0, 8, 4, 12, 6, 14)
 *  // std::vector(7, 15, 10, 2, 16, 1)
 *  // std::vector(17, 9, 13, 5, 11, 3)
 *
 *  // Iteration 2 with 2 subset(s)
 *  // std::vector(17, 10, 5, 13, 3, 11, 2, 16, 1)
 *  // std::vector(0, 8, 4, 12, 6, 14, 7, 15, 9)
 *
 *  // Iteration 3 with 2 subset(s)
 *  // std::vector(17, 10, 5, 13, 3, 11, 2, 16, 1)
 *  // std::vector(0, 8, 4, 12, 6, 14, 7, 15, 9)
 * \endcode
 */
class DefaultSubsetGenerator : public TransitionSchemeExtension
{
    CTL_TYPE_ID(1200)

public:
    enum Order { Random, Adjacent, Orthogonal180, Orthogonal360, RealOrthogonal };

    DefaultSubsetGenerator();

    // additional settings
    void setFixedNumberOfSubsets(uint nbSubsets);
    void setOrder(DefaultSubsetGenerator::Order order);
};



} // namespace CTL

#endif // CTL_DEFAULTSUBSETGENERATOR_H
