#include "oclregularizers.h"

#include <stdexcept>

namespace CTL {
namespace OCL {

DECLARE_SERIALIZABLE_TYPE(TVRegularizer)
DECLARE_SERIALIZABLE_TYPE(HuberRegularizer)

/*!
 * \brief Creates a TVRegularizer with regularization parameter set to \a regularizationStrength.
 *
 * The parameter \a regularizationStrength defines the maximum change in multiples of 10 Hounsfield
 * units (HU, at 50 keV) that can be applied to a single voxel in one execution of filter().
 * [1.0 means the maximum change is +-10 HU]
 */
TVRegularizer::TVRegularizer(float regularizationStrength)
    : GenericOCLVolumeFilter("recon/tv_regularizer.cl", { regularizationStrength * 10.0f })
{
}

/*!
 * \brief Sets the regularization parameter to \a strengh.
 *
 * The parameter \a strength defines the maximum change in multiples of 10 Hounsfield units (HU,
 * at 50 keV) that can be applied to a single voxel in one execution of filter().
 * [1.0 means the maximum change is +-10 HU]
 */
void TVRegularizer::setRegularizationStrength(float strength)
{
    setAdditionalKernelArg(strength * 10.0f);
}

/*!
 * \brief Creates a HuberRegularizer with a given set of parameters set.
 *
 * The parameters define the following things:
 * - \a regularizationStrength linearly scales the portion of the gradient that is applied to change
 * a single voxel's value in one execution of filter()
 * [1.0 means 1% of the gradient is applied].
 * - \a huberEdge defines the (absolute) difference threshold in multiples of 100 HU (at 50 keV) to
 * which differences will be clamped for computation of the gradient. For differences above the
 * threshold, the regularization effect is comparable to TV minimization
 * [1.0 means the Huber edges are +-100 HU].
 * - \a weightZ and \a weightXY specify the relative strength of the regularization in Z- and XY-
 * plane, respectively (in most settings, this refers to between-plane and in-plane regularization)
 * [\a weightZ = \a weightXY means all dimensions are treated equally].
 * - \a directZNeighborWeight is a separate multiplicative weight factor for the two voxels that are
 * direct Z-neighbors of the voxel of interest
 * [1.0 means the direct Z-Neighbors are treated the same as all other Z-Neighbors].
 *
 * Compared to a direct call of the regularizer kernel through GenericOCLRegularizer, the parameters
 * have the following translation:
 * \code
 * GenericOCLRegularizer("recon/huber_regularizer.cl", { regularizationStrength * 0.01f, huberEdge * 100.0f,
 *                                                       weightZ, weightXY, directZNeighborWeight })
 * \endcode
 */
HuberRegularizer::HuberRegularizer(float regularizationStrength, float huberEdge,
                                   float weightZ, float weightXY, float directZNeighborWeight)
    : GenericOCLVolumeFilter("recon/huber_regularizer.cl", { regularizationStrength * 0.01f, huberEdge * 100.0f,
                                                             weightZ, weightXY, directZNeighborWeight })
{
}

/*!
 * \brief Sets the regularization parameter to \a strength.
 *
 * The parameter \a strength linearly scales the portion of the gradient that is applied to change
 * a single voxel's value in one execution of filter()
 * [\a strength=1.0 means 1% of the gradient is applied].
 */
void HuberRegularizer::setRegularizationStrength(float strength)
{
    /*
     * check if the first kernel argument is available; can only be violated if
     * setAdditionalKernelArgs() was used (illegaly) through a base class pointer.
     */
    if(_additionalArgs.size() < 1)
        throw std::runtime_error("HuberRegularizer::setRegularizationStrength(): "
                                 "Kernel arguments had beed invalidated. "
                                 "Did you (illegaly) use setAdditionalKernelArgs()");

    _additionalArgs[0] = strength * 0.01f;
    setAdditionalKernelArgs(_additionalArgs);
}

} // namespace OCL
} // namespace CTL

