#ifndef CTL_DATAMODELOPERATIONS_H
#define CTL_DATAMODELOPERATIONS_H

#include "abstractdatamodel.h"
#include "abstractdatamodel2d.h"

namespace CTL {

class AbstractDataModelOperation : public AbstractDataModel
{
public:
    AbstractDataModelOperation(std::shared_ptr<AbstractDataModel> lhs,
                               std::shared_ptr<AbstractDataModel> rhs);

    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

    ~AbstractDataModelOperation() override = default;

protected:
    std::shared_ptr<AbstractDataModel> _lhs;
    std::shared_ptr<AbstractDataModel> _rhs;

    AbstractDataModelOperation() = default;
    AbstractDataModelOperation(const AbstractDataModelOperation&) = default;
    AbstractDataModelOperation(AbstractDataModelOperation&&) = default;
    AbstractDataModelOperation& operator=(const AbstractDataModelOperation&) = default;
    AbstractDataModelOperation& operator=(AbstractDataModelOperation&&) = default;
};

class DataModelAdd : public AbstractDataModelOperation
{
    CTL_TYPE_ID(1)

    // abstract interface
    public:virtual float valueAt(float position) const override;
    public:virtual AbstractDataModel* clone() const override;

public:
    using AbstractDataModelOperation::AbstractDataModelOperation;
};

class DataModelSub : public AbstractDataModelOperation
{
    CTL_TYPE_ID(2)

    // abstract interface
    public:virtual float valueAt(float position) const override;
    public:virtual AbstractDataModel* clone() const override;

public:
    using AbstractDataModelOperation::AbstractDataModelOperation;
};

class DataModelMul : public AbstractDataModelOperation
{
    CTL_TYPE_ID(3)

    // abstract interface
    public:virtual float valueAt(float position) const override;
    public:virtual AbstractDataModel* clone() const override;

public:
    using AbstractDataModelOperation::AbstractDataModelOperation;
};

class DataModelDiv : public AbstractDataModelOperation
{
    CTL_TYPE_ID(4)

    // abstract interface
    public:virtual float valueAt(float position) const override;
    public:virtual AbstractDataModel* clone() const override;

public:
    using AbstractDataModelOperation::AbstractDataModelOperation;
};

class DataModelCat : public AbstractDataModelOperation
{
    CTL_TYPE_ID(5)

    // abstract interface
    public:virtual float valueAt(float position) const override;
    public:virtual AbstractDataModel* clone() const override;

public:
    using AbstractDataModelOperation::AbstractDataModelOperation;
};


// #########
// 2D models
class AbstractDataModel2DOperation : public AbstractDataModel2D
{
public:
    AbstractDataModel2DOperation(std::shared_ptr<AbstractDataModel2D> lhs,
                                 std::shared_ptr<AbstractDataModel2D> rhs);

    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

    ~AbstractDataModel2DOperation() override = default;

protected:
    std::shared_ptr<AbstractDataModel2D> _lhs;
    std::shared_ptr<AbstractDataModel2D> _rhs;

    AbstractDataModel2DOperation() = default;
    AbstractDataModel2DOperation(const AbstractDataModel2DOperation&) = default;
    AbstractDataModel2DOperation(AbstractDataModel2DOperation&&) = default;
    AbstractDataModel2DOperation& operator=(const AbstractDataModel2DOperation&) = default;
    AbstractDataModel2DOperation& operator=(AbstractDataModel2DOperation&&) = default;
};

class DataModel2DAdd : public AbstractDataModel2DOperation
{
    CTL_TYPE_ID(1)

    // abstract interface
    public:virtual float valueAt(float x, float y) const override;
    public:virtual AbstractDataModel2D* clone() const override;

public:
    using AbstractDataModel2DOperation::AbstractDataModel2DOperation;
};

class DataModel2DSub : public AbstractDataModel2DOperation
{
    CTL_TYPE_ID(2)

    // abstract interface
    public:virtual float valueAt(float x, float y) const override;
    public:virtual AbstractDataModel2D* clone() const override;

public:
    using AbstractDataModel2DOperation::AbstractDataModel2DOperation;
};

class DataModel2DMul : public AbstractDataModel2DOperation
{
    CTL_TYPE_ID(3)

    // abstract interface
    public:virtual float valueAt(float x, float y) const override;
    public:virtual AbstractDataModel2D* clone() const override;

public:
    using AbstractDataModel2DOperation::AbstractDataModel2DOperation;
};

class DataModel2DDiv : public AbstractDataModel2DOperation
{
    CTL_TYPE_ID(4)

    // abstract interface
    public:virtual float valueAt(float x, float y) const override;
    public:virtual AbstractDataModel2D* clone() const override;

public:
    using AbstractDataModel2DOperation::AbstractDataModel2DOperation;
};


} // namespace CTL

#endif // CTL_DATAMODELOPERATIONS_H
