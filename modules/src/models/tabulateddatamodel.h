#ifndef CTL_TABULATEDDATAMODEL_H
#define CTL_TABULATEDDATAMODEL_H

#include "abstractdatamodel.h"
#include <QMap>

namespace CTL {

class PointSeriesBase;

/*!
 * \class TabulatedDataModel
 * \brief The TabulatedDataModel class is a data model that handles values in a lookup table.
 *
 * Sub-classes must implement the method to sample a value at a given position (valueAt()).
 *
 * Parameters can be set by passing a QVariant that contains all necessary information.
 * Re-implement the setParameter() method to parse the QVariant into your required format within
 * sub-classes of TabulatedDataModel.
 *
 * Example:
 * \code
 *  auto table = TabulatedDataModel();
 *  table.insertDataPoint(5.0f, 1.0f);
 *  table.insertDataPoint(10.0f, 3.0f);
 *  table.insertDataPoint(15.0f, 10.0f);
 *  table.insertDataPoint(20.0f, 7.7f);
 *
 *  // get individual values (lin. interpolation)
 *  qInfo() << table.valueAt(7.5f);             // output: 2
 *  qInfo() << table.valueAt(14.5f);            // output: 9.3
 *
 *  // get bin integrals (trapezoidal rule)
 *  qInfo() << table.binIntegral(7.5f, 5.0f);   // output: 10
 *  qInfo() << table.binIntegral(10.0f, 20.0f); // output: 89.25
 * \endcode
 */
class TabulatedDataModel : public AbstractIntegrableDataModel
{
    CTL_TYPE_ID(30)

    // abstract interfaces
    public: float valueAt(float position) const override;
    public: float binIntegral(float position, float binWidth) const override;
    public: AbstractDataModel* clone() const override;

public:
    TabulatedDataModel();
    explicit TabulatedDataModel(const PointSeriesBase& dataSeries);
    explicit TabulatedDataModel(QMap<float, float> table);
    TabulatedDataModel(const QVector<float>& keys, const QVector<float>& values);

    // getter methods
    const QMap<float, float>& lookupTable() const;

    // setter methods
    void clearData();
    void setData(const PointSeriesBase& dataSeries);
    void setData(QMap<float, float> table);
    void setData(const QVector<float>& keys, const QVector<float>& values);

    // other methods
    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

    void insertDataPoint(float key, float value);

private:
    QMap<float, float> _data;

    QVariantList dataAsVariantList() const;
    void setDataFromVariantList(const QVariantList& list);
};

} // namespace CTL

#endif // CTL_TABULATEDDATAMODEL_H
