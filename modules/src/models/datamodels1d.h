#ifndef CTL_DATAMODELS1D_H
#define CTL_DATAMODELS1D_H

#include "abstractdatamodel.h"
#include <cfloat>

namespace CTL {

/*!
 * \class SaturatedLinearModel
 * \brief The SaturatedLinearModel is a data model to map values according to a linear central
 * segment that connects two constant regimes (saturation).
 *
 * The model is defined by two parameters:
 * \li lower saturation level \f$ a \f$
 * \li upper saturation level \f$ b \f$
 *
 * This is illustrated in the following figure.
 * ![Illustration of a SaturatedLinearModel with parameters (2, 8)](linearSaturation.svg)
 *
 * \sa DetectorSaturationLinearModel
 */
class SaturatedLinearModel : public AbstractIntegrableDataModel
{
    CTL_TYPE_ID(10)

    public: float valueAt(float position) const override;
    public: float binIntegral(float position, float binWidth) const override;
    public: AbstractDataModel* clone() const override;

public:
    explicit SaturatedLinearModel(float lowerCap = 0.0f, float upperCap = FLT_MAX);

    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

private:
    float _a = 0.0f;    //!< lower saturation level
    float _b = FLT_MAX; //!< upper saturation level

    void setParFromList(const QVariantList& list);
    void setParFromMap(const QVariantMap& map);
};

/*!
 * \class SaturatedSplineModel
 * \brief The SaturatedSplineModel is a data model to map values according to a dependency composed
 * of a linear central segment and a fade in/out from/to a constant level by quadratic splines.
 *
 * The model is defined by four parameters:
 * \li lower saturation level \f$ a \f$
 * \li upper saturation level \f$ b \f$
 * \li softness of lower transition \f$ s_a \f$
 * \li softness of upper transition \f$ s_b \f$
 *
 * This is illustrated in the following figure.
 * ![Illustration of a SaturatedSplineModel with parameters (2, 8, 0.5, 2)](splineSaturation.svg)
 *
 * \sa DetectorSaturationSplineModel
 */
class SaturatedSplineModel : public AbstractDataModel
{
    CTL_TYPE_ID(20)

    public: float valueAt(float position) const override;
    public: AbstractDataModel* clone() const override;

public:
    explicit SaturatedSplineModel(float lowerCap = 0.0f, float upperCap = FLT_MAX, float softening = 0.0f);
    SaturatedSplineModel(float lowerCap, float upperCap, float softLower, float softUpper);

    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

private:
    float _a = 0.0f;     //!< lower saturation level
    float _b = FLT_MAX;  //!< upper saturation level
    float _softA = 0.0f; //!< softening margin of lower saturation
    float _softB = 0.0f; //!< softening margin of upper saturation

    void setParFromList(const QVariantList& list);
    void setParFromMap(const QVariantMap& map);

    float spline1(float x) const;
    float spline2(float x) const;
};


// ####################################
// Step functions & other simple models
// ####################################

/*
 * All step functions are implemented with left-closed and right-open intervals,
 * i.e. they are piecewise constant on intervals of the form [a,b).
 * This allows seamless compositions of these models.
 */

/*!
 * \class StepFunctionModel
 * \brief The StepFunctionModel class represents a step (Heaviside) function.
 *
 * This model maps input values according to a step function. The direction, position, and height
 * of the step can be specified in the constructor of using setParameter().
 */
class StepFunctionModel : public AbstractIntegrableDataModel
{
    CTL_TYPE_ID(50)

    public: float valueAt(float position) const override;
    public: float binIntegral(float position, float binWidth) const override;
    public: AbstractDataModel* clone() const override;

public:
    enum StepDirection{ RightIsZero = 0, Downwards = 0, LeftIsZero = 1, Upwards = 1 };

    explicit StepFunctionModel(float threshold = 0.0f, float amplitude = 1.0f, StepDirection stepDirection = LeftIsZero);

    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

private:
    float _threshold;
    float _amplitude;
    StepDirection _stepDirection;
};

/*!
 * \class ConstantModel
 * \brief The ConstantModel class represents a model that always returns a fixed value.
 *
 * This model maps all input to the same output value specified in the constructor.
 */
class ConstantModel : public AbstractIntegrableDataModel
{
    CTL_TYPE_ID(51)

    public: float valueAt(float position) const override;
    public: float binIntegral(float position, float binWidth) const override;
    public: AbstractDataModel* clone() const override;

public:
    explicit ConstantModel(float amplitude = 1.0f);

    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

private:
    float _amplitude;
};

/*!
 * \class RectFunctionModel
 * \brief The RectFunctionModel class represents a rectangular (rect) function.
 *
 * This model maps input values according to a rect function. The start and end point of the
 * rectangle, as well as its height can be specified in the constructor of using setParameter().
 */
class RectFunctionModel : public AbstractDataModel
{
    CTL_TYPE_ID(52)

    public: float valueAt(float position) const override;
    public: AbstractDataModel* clone() const override;

public:
    explicit RectFunctionModel(float rectBegin = -0.5f, float rectEnd = 0.5f, float amplitude = 1.0f);

    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

private:
    float _rectBegin;
    float _rectEnd;
    float _amplitude;
};

/*!
 * \class IdentityModel
 * \brief The IdentityModel class represents the identity function.
 *
 * This model always returns the input unchanged (identity).
 */
class IdentityModel : public AbstractIntegrableDataModel
{
    CTL_TYPE_ID(53)

    public: float valueAt(float position) const override;
    public: float binIntegral(float position, float binWidth) const override;
    public: AbstractDataModel* clone() const override;
};

/*!
 * \class GaussianModel1D
 * \brief The GaussianModel1D class represents a one-dimensional Gaussian curve.
 *
 * This model maps input values according to a one-dimensional Gaussian function.
 */
class GaussianModel1D : public AbstractIntegrableDataModel
{
    CTL_TYPE_ID(54)

    // AbstractDataModel2D interface
    public: float valueAt(float position) const override;
    public: float binIntegral(float position, float binWidth) const override;
    public: AbstractDataModel* clone() const override;

public:
    explicit GaussianModel1D(float amplitude = 1.0f, float mean = 0.0f, float std = 1.0f);

    // SerializationInterface interface
    QVariant toVariant() const override;
    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

private:
    float _ampl = 1.0f;
    float _mean = 0.0f;
    float _std = 1.0f;
};

// typedefs
typedef SaturatedLinearModel DetectorSaturationLinearModel;
typedef SaturatedSplineModel DetectorSaturationSplineModel;

} // namespace CTL

/*! \file */
///@{
/*!
 * \typedef CTL::DetectorSaturationLinearModel
 * \brief Alias for SaturatedLinearModel used here to map true (or expected) values to actually
 * measured values.
 *
 * \relates CTL::SaturatedLinearModel
 */

/*!
 * \typedef CTL::DetectorSaturationSplineModel
 * \brief Alias for SaturatedSplineModel used here to map true (or expected) values to actually
 * measured values.
 *
 * \relates CTL::SaturatedSplineModel
 */
///@}

#endif // CTL_DATAMODELS1D_H
