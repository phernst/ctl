#include "datamodeloperations.h"
#include <stdexcept>

namespace CTL {

DECLARE_SERIALIZABLE_TYPE(DataModelAdd)
DECLARE_SERIALIZABLE_TYPE(DataModelSub)
DECLARE_SERIALIZABLE_TYPE(DataModelMul)
DECLARE_SERIALIZABLE_TYPE(DataModelDiv)
DECLARE_SERIALIZABLE_TYPE(DataModelCat)
DECLARE_SERIALIZABLE_TYPE(DataModel2DAdd)
DECLARE_SERIALIZABLE_TYPE(DataModel2DSub)
DECLARE_SERIALIZABLE_TYPE(DataModel2DMul)
DECLARE_SERIALIZABLE_TYPE(DataModel2DDiv)

CTL::AbstractDataModelOperation::AbstractDataModelOperation(std::shared_ptr<AbstractDataModel> lhs,
                                                            std::shared_ptr<AbstractDataModel> rhs)
    : _lhs(std::move(lhs))
    , _rhs(std::move(rhs))
{
    if(!_lhs || !_rhs)
        throw std::runtime_error("AbstractDataModelOperation: Unable to construct data model operation."
                                 "At least one of the operands is nullptr.");
}

QVariant AbstractDataModelOperation::parameter() const
{
    QVariantMap ret;
    ret.insert(QStringLiteral("LHS model"), _lhs->toVariant());
    ret.insert(QStringLiteral("RHS model"), _rhs->toVariant());

    return ret;
}

void AbstractDataModelOperation::setParameter(const QVariant& parameter)
{
    QVariantMap parMap = parameter.toMap();
    _lhs.reset(SerializationHelper::parseDataModel(parMap.value(QStringLiteral("LHS model"))));
    _rhs.reset(SerializationHelper::parseDataModel(parMap.value(QStringLiteral("RHS model"))));
}

float DataModelAdd::valueAt(float position) const
{
    return _lhs->valueAt(position) + _rhs->valueAt(position);
}

float DataModelSub::valueAt(float position) const
{
    return _lhs->valueAt(position) - _rhs->valueAt(position);
}

float DataModelMul::valueAt(float position) const
{
    return _lhs->valueAt(position) * _rhs->valueAt(position);
}

float DataModelDiv::valueAt(float position) const
{
    return _lhs->valueAt(position) / _rhs->valueAt(position);
}

float DataModelCat::valueAt(float position) const
{
    return _rhs->valueAt(_lhs->valueAt(position));
}

AbstractDataModel* DataModelAdd::clone() const { return new DataModelAdd(*this); }

AbstractDataModel* DataModelSub::clone() const { return new DataModelSub(*this); }

AbstractDataModel* DataModelMul::clone() const { return new DataModelMul(*this); }

AbstractDataModel* DataModelDiv::clone() const { return new DataModelDiv(*this); }

AbstractDataModel* DataModelCat::clone() const { return new DataModelCat(*this); }


// #########
// 2D models
AbstractDataModel2DOperation::AbstractDataModel2DOperation(std::shared_ptr<AbstractDataModel2D> lhs,
                                                           std::shared_ptr<AbstractDataModel2D> rhs)
    : _lhs(std::move(lhs))
    , _rhs(std::move(rhs))
{
    if(!_lhs || !_rhs)
        throw std::runtime_error("AbstractDataModel2DOperation: Unable to construct data model operation."
                                 "At least one of the operands is nullptr.");
}

QVariant AbstractDataModel2DOperation::parameter() const
{
    QVariantMap ret;
    ret.insert(QStringLiteral("LHS model"), _lhs->toVariant());
    ret.insert(QStringLiteral("RHS model"), _rhs->toVariant());

    return ret;
}

void AbstractDataModel2DOperation::setParameter(const QVariant& parameter)
{
    QVariantMap parMap = parameter.toMap();
    _lhs.reset(SerializationHelper::parseDataModel2D(parMap.value(QStringLiteral("LHS model"))));
    _rhs.reset(SerializationHelper::parseDataModel2D(parMap.value(QStringLiteral("RHS model"))));
}

float DataModel2DAdd::valueAt(float x, float y) const
{
    return _lhs->valueAt(x, y) + _rhs->valueAt(x, y);
}

float DataModel2DSub::valueAt(float x, float y) const
{
    return _lhs->valueAt(x, y) - _rhs->valueAt(x, y);
}

float DataModel2DMul::valueAt(float x, float y) const
{
    return _lhs->valueAt(x, y) * _rhs->valueAt(x, y);
}

float DataModel2DDiv::valueAt(float x, float y) const
{
    return _lhs->valueAt(x, y) / _rhs->valueAt(x, y);
}

AbstractDataModel2D* DataModel2DAdd::clone() const { return new DataModel2DAdd(*this); }

AbstractDataModel2D* DataModel2DSub::clone() const { return new DataModel2DSub(*this); }

AbstractDataModel2D* DataModel2DMul::clone() const { return new DataModel2DMul(*this); }

AbstractDataModel2D* DataModel2DDiv::clone() const { return new DataModel2DDiv(*this); }

} // namespace CTL
