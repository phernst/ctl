#include "growingvesselphantom.h"

namespace CTL {

GrowingVesselPhantom::GrowingVesselPhantom(VoxelVolume<float> initalVolume)
    : AbstractDynamicVolumeData(std::move(initalVolume))
{
    if(!this->hasData())
        this->fill(0.0f);
}

VesselPhantomGenerator::Parameters& GrowingVesselPhantom::generatorSettings() { return _par; }

double GrowingVesselPhantom::speed() const { return _speed; }

uint GrowingVesselPhantom::seed() const { return _seed; }

void GrowingVesselPhantom::setSeed(const uint& seed) { _seed = seed; }

void GrowingVesselPhantom::setSpeed(double speed) { _speed = speed; }

void GrowingVesselPhantom::updateVolume()
{
    VesselPhantomGenerator gen;
    gen.parameters() = _par;
    gen.setSeed(_seed);
    gen.setMaxNbIter(static_cast<uint64_t>(_speed * time() + 1.0));
    gen.drawPhantom(*this);
}

SpectralVolumeData* GrowingVesselPhantom::clone() const { return new GrowingVesselPhantom(*this); }

} // namespace CTL
