#ifndef CTL_ABSTRACTVOLUMEFILTER_H
#define CTL_ABSTRACTVOLUMEFILTER_H

#include "io/serializationinterface.h"

namespace CTL {

template<typename>
class VoxelVolume;

class AbstractVolumeFilter : public SerializationInterface
{
    CTL_TYPE_ID(2000)

public:virtual void filter(VoxelVolume<float>& volume) = 0;

public:
    virtual ~AbstractVolumeFilter() = default;

    virtual QVariant parameter() const;
    virtual void setParameter(const QVariant& parameter);

    void fromVariant(const QVariant& variant) override;
    QVariant toVariant() const override;

protected:
    AbstractVolumeFilter() = default;
    AbstractVolumeFilter(const AbstractVolumeFilter&) = default;
    AbstractVolumeFilter(AbstractVolumeFilter&&) = default;
    AbstractVolumeFilter& operator=(const AbstractVolumeFilter&) = default;
    AbstractVolumeFilter& operator=(AbstractVolumeFilter&&) = default;
};

} // namespace CTL

#endif // CTL_ABSTRACTVOLUMEFILTER_H
