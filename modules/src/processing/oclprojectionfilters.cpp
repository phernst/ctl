#include "oclprojectionfilters.h"

#include "acquisition/geometryencoder.h"
#include "img/projectiondata.h"
#include "mat/pi.h"
#include "mat/matrix_algorithm.h"
#include "processing/range.h"

#include <QMetaEnum>

namespace {

/*!
 * \brief Converts a vector of std::complex<double> values int a vector of floats by keeping only
 * the real part of the complex number.
 */
std::vector<float> vector_complex_to_real(const std::vector<std::complex<double>>& fourier)
{
    std::vector<float> real_vector;
    real_vector.reserve(fourier.size());
    std::transform(
        fourier.cbegin(), fourier.cend(),
        std::back_inserter(real_vector),
        [](const std::complex<double>& c) { return c.real(); }
    );
    return real_vector;
}

} // namespace

namespace CTL {
namespace OCL {

DECLARE_SERIALIZABLE_TYPE(CosineWeighting)
DECLARE_SERIALIZABLE_TYPE(ParkerWeightingRev)
DECLARE_SERIALIZABLE_TYPE(ApodizationFilter)
DECLARE_SERIALIZABLE_TYPE(RamLakFilter)

/*!
 * \brief Constructs an instance of CosineWeighting.
 */
CosineWeighting::CosineWeighting()
    : GenericOCLProjectionFilter("processing/cosine_weighting.cl")
{
}

/*!
 * \brief Applies cosine weighting to \a projections assuming the (pre-encoded) geometry of
 * \a pMats.
 */
void CosineWeighting::filter(ProjectionData& projections, const FullGeometry& pMats)
{
    std::vector<float> Kmats(9 * pMats.totalNbPmats());
    auto iter = Kmats.begin();

    for(const auto& viewPmats : pMats)
        for(const auto& modPmat : viewPmats)
        {
            auto K = modPmat.intrinsicMatK();
            std::copy_n(K.constBegin(), 9, iter);
            iter += 9;
        }
    try // OpenCL exception handling
    {
        const auto& context = _queue.getInfo<CL_QUEUE_CONTEXT>();

        auto kBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(float) * Kmats.size());
        _queue.enqueueWriteBuffer(kBuffer, CL_FALSE, 0, sizeof(float) * Kmats.size(), Kmats.data());
        _kernel->setArg(3, kBuffer);

        GenericOCLProjectionFilter::filter(projections);

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";
    }

}

/*!
 * \brief Applies cosine weighting to \a projections assuming the geometry defined by \a setup.
 */
void CosineWeighting::filter(ProjectionData& projections, const AcquisitionSetup& setup)
{
    filter(projections, GeometryEncoder::encodeFullGeometry(setup));
}

/*!
 * \brief Returns \c false, since this class does not use automated combining.
 *
 * \sa GenericOCLProjectionFilter::autoCombine().
 */
bool CosineWeighting::autoCombine() const
{
    return false;
}

/*!
 * \copybrief GenericOCLProjectionFilter::globalWorksize
 *
 * This returns a three-dimensional \c cl::NDRange, where the three dimensions correspond to:
 * - number of channels,
 * - number of rows, and
 * - number of modules
 * in \a projections, respectively.
 */
cl::NDRange CosineWeighting::globalWorksize(const ProjectionData& projections) const
{
    const auto U = projections.dimensions().nbChannels;
    const auto V = projections.dimensions().nbRows;
    const auto M = projections.dimensions().nbModules;
    return cl::NDRange(U, V, M);
}

/*!
 * \brief Constructs an instance of ParkerWeightingRev with q-value \a q.
 *
 * For more details on the q-value, please refer to the detailed class documentation.
 */
ParkerWeightingRev::ParkerWeightingRev(float q)
    : GenericOCLProjectionFilter("processing/rev_parker_weighting.cl")
    , _q(q)
{
}

/*!
 * \brief Applies (revised) Parker weighting to \a projections assuming the (pre-encoded) geometry
 * of \a pMats.
 */
void ParkerWeightingRev::filter(ProjectionData& projections, const FullGeometry& pMats)
{
    if(pMats.nbViews() != pMats.totalNbPmats())
        qWarning() << "Trying to apply Parker weights to multi-module detector data. "
                      "Result is undefined.";
    if(pMats.nbViews() != projections.nbViews())
        throw std::domain_error("ParkerWeighting: Number of views in projections does not match "
                                "number of views in pMats.");


    const auto nbViews = pMats.nbViews();
    if(nbViews < 2)
    {
        qWarning() << "ParkerWeightingRev: Skipped filtering. "
                      "Reason: trying to apply Parker weights to data with less than two views.";
        return;
    }

    std::vector<float> fx(nbViews);
    std::vector<float> px(nbViews);
    std::vector<float> alpha(nbViews);
    double alphaAccumulator;

    const auto& P0 = pMats.first().first();
    const auto U = projections.dimensions().nbChannels;
    const auto V = projections.dimensions().nbRows;
    const auto fanAngle = float(std::acos(mat::dot(P0.directionSourceToPixel(0, V/2.0-0.5, ProjectionMatrix::NormalizeAsUnitVector),
                                                   P0.directionSourceToPixel(U, V/2.0-0.5, ProjectionMatrix::NormalizeAsUnitVector))));

    int rotDirection, lastRotDirection = 0;
    auto lastPrincipRay = P0.principalRayDirection();
    // first view
    const auto K0 = P0.intrinsicMatK();
    fx[0] = K0.get<0,0>();
    px[0] = K0.get<0,2>();
    alpha[0] = 0.0f; // define first angle to be zero
    alphaAccumulator = 0.0;
    for(uint v = 1; v < nbViews; ++v)
    {
        const auto& P = pMats.view(v).first();
        const auto K = P.intrinsicMatK();
        const auto principRay = P.principalRayDirection();
        fx[v] = K.get<0,0>();
        px[v] = K.get<0,2>();
        // add angular difference between adjacent views to previous angle
        const auto rotVec = cross(lastPrincipRay, principRay);
        alphaAccumulator += std::asin(rotVec.norm());
        alpha[v] = float(alphaAccumulator);

        // check if rotation vector is anti-parallel to detector y-axis (required in kernel)
        rotDirection = dot(rotVec, P.rotationMatR().row<1>()) > 0.0
                       ? -1 // -> clockwise rotation
                       : 1; // -> counterclockwise rotation

        if(lastRotDirection && rotDirection != lastRotDirection)
            throw std::domain_error("ParkerWeightingRev::filter: rotational direction must not "
                                    "change during acquisition");

        lastRotDirection = rotDirection;
        lastPrincipRay = principRay;
    }

    _lastAngularRange = alpha.back();

    // shift angles by one angular step on front and back (avoid zero-weighted projections)
    const auto angularStep = 0.5f * _lastAngularRange / (nbViews - 1);
    std::transform(alpha.begin(), alpha.end(), alpha.begin(),
                   [angularStep] (float a) { return a + angularStep; });

    const auto overscan = alpha.back() - (float(PI) + fanAngle) + angularStep;
    qDebug() << "ParkerWeightingRev: overscanning" << (overscan - 2.0f * angularStep) * 180.0f / float(PI) << "deg";

    try // OpenCL exception handling
    {
        const auto& context = _queue.getInfo<CL_QUEUE_CONTEXT>();

        auto fxBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(float) * nbViews);
        auto pxBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(float) * nbViews);
        auto alphaBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(float) * nbViews);
        _queue.enqueueWriteBuffer(fxBuffer, CL_FALSE, 0, sizeof(float) * nbViews, fx.data());
        _queue.enqueueWriteBuffer(pxBuffer, CL_FALSE, 0, sizeof(float) * nbViews, px.data());
        _queue.enqueueWriteBuffer(alphaBuffer, CL_FALSE, 0, sizeof(float) * nbViews, alpha.data());
        _kernel->setArg(3, fanAngle);
        _kernel->setArg(4, overscan);
        _kernel->setArg(5, _q);
        _kernel->setArg(6, rotDirection);
        _kernel->setArg(7, fxBuffer);
        _kernel->setArg(8, pxBuffer);
        _kernel->setArg(9, alphaBuffer);

        GenericOCLProjectionFilter::filter(projections);

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";
    }
}

/*!
 * \brief Applies (revised) Parker weighting to \a projections assuming the geometry defined by
 * \a setup.
 */
void ParkerWeightingRev::filter(ProjectionData& projections, const AcquisitionSetup& setup)
{
    filter(projections, GeometryEncoder::encodeFullGeometry(setup));
}

/*!
 * \brief Returns the angular range (in radiants) that has been determined for the last projection
 * dataset that the filter has been applied on.
 *
 * Example:
 * \code
 *  // create an AcquisitionSetup with 10 views
 *  const auto nbViews = 10;
 *  AcquisitionSetup setup(makeCTSystem<blueprints::GenericCarmCT>(), nbViews);
 *  // apply a short scan trajectory covering 225 degrees angular range
 *  setup.applyPreparationProtocol(protocols::ShortScanTrajectory(700.0, 0.0, 225.0_deg));
 *
 *  // create some dummy projection data
 *  ProjectionData proj(setup.system()->detector()->viewDimensions());
 *  proj.allocateMemory(nbViews);
 *  proj.fill(0.0f);
 *
 *  // create the ParkerWeightingRev filter
 *  OCL::ParkerWeightingRev filt;
 *
 *  qInfo() << filt.lastAngularRange();                 // output: 0 (no prev. filtering)
 *
 *  // filter the dummy projections
 *  filt.filter(proj, setup);
 *
 *  qInfo() << filt.lastAngularRange() * 180.0f / PI;   // output: 225
 * \endcode
 */
float ParkerWeightingRev::lastAngularRange() const { return _lastAngularRange; }

/**
 * \brief Constructs an ApodizationFilter object.
 *
 * \param filterType Type of the apodization filter
 * \param frequencyScaling Scaling of the apodization function along frequency axis.
 *           Can take values in \f$(0, 1]\f$. Frequencies above/below
 *           the cutoff frequency calculated from this scaling factor
 *           are set to zero.
 *                         
 */
ApodizationFilter::ApodizationFilter(FilterType filterType, float frequencyScaling)
    : GenericOCLProjectionFilter("processing/apodization_filter.cl", { 1.0f })
    , _filterType(filterType)
    , _frequencyScaling(frequencyScaling)
    , _filter()
{
}

/*!
 * \brief Returns the parameters of this instance as a QVariant.
 *
 * Returns a QVariantMap including the following (key, value)-pairs:
 *
 * - information from the base class (GenericOCLProjectionFilter),
 * - ("frequency scaling", [float] scaling of the apodization function),
 * - ("filter type", [string] name of the filter type).
 */
QVariant ApodizationFilter::parameter() const
{
    QVariantMap ret = GenericOCLProjectionFilter::parameter().toMap();

    ret.insert(QStringLiteral("frequency scaling"), _frequencyScaling);
    ret.insert(QStringLiteral("filter type"), metaEnum().valueToKey(_filterType));

    return ret;
}

/*!
 * \brief Sets the parameters of this instance based on the passed QVariant \a parameter.
 *
 * The passed \a parameter must be a QVariantMap containing one or more of the following
 * (key, value)-pairs:
 *
 * - information from the base class (GenericOCLProjectionFilter),
 * - ("frequency scaling", [float] scaling of the apodization function),
 * - ("filter type", [string] name of the filter type).
 *
 * Note that changing parameters of the base class this way (esp. changing the OpenCL kernel file)
 * leads to undefined behavior.
 */
void ApodizationFilter::setParameter(const QVariant& parameter)
{
    GenericOCLProjectionFilter::setParameter(parameter);

    const QVariantMap parMap = parameter.toMap();

    if(parMap.contains(QStringLiteral("frequency scaling")))
        _frequencyScaling = parMap.value(QStringLiteral("frequency scaling")).toFloat();
    if(parMap.contains(QStringLiteral("filter type")))
    {
        const auto typeStr = parMap.value(QStringLiteral("filter type")).toString();
        int type = metaEnum().keyToValue(qPrintable(typeStr));
        if(type == -1)
        {
            qWarning() << "ApodizationFilter::setParameter: Incorrect filter type specified. "
                          " (valid options: [RamLak | SheppLogan | Cosine | Hann]). "
                          "Using default value (RamLak).";
            type = 1;
        }

        _filterType = static_cast<FilterType>(type);
    }

    // clear filter to ensure re-computation at next call of filter()
    _filter.clear();
}

/*!
 * \brief Default ctor; private. Used for purpose of deserialization only.
 */
ApodizationFilter::ApodizationFilter()
    : ApodizationFilter(RamLak)
{
}

/*!
 * \brief Applies the apodization filter operation to \a projections.
 */
void ApodizationFilter::filter(ProjectionData& projections)
{
    // generate coefficients if projection size has changed
    if (2*projections.dimensions().nbChannels != _filter.size())
    {
        _filter = generateCoefficients(2*projections.dimensions().nbChannels);
    }

    try // OpenCL exception handling
    {
        const auto& context = _queue.getInfo<CL_QUEUE_CONTEXT>();

        auto kBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(float) * _filter.size());
        _queue.enqueueWriteBuffer(kBuffer, CL_FALSE, 0, sizeof(float) * _filter.size(), _filter.data());
        _kernel->setArg(4, kBuffer);

        GenericOCLProjectionFilter::filter(projections);

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";
    }
}

/*!
 * \brief Generates the coefficients for a filter of size \a filterSize.
 */
std::vector<float> ApodizationFilter::generateCoefficients(unsigned filterSize) const
{
    if(_frequencyScaling <= 0.0f || _frequencyScaling > 1.0f)
    {
        throw std::runtime_error(
            "ApodizationFilter::generateCoefficients: "
            "invalid frequency scaling; "
            "must be in (0, 1].");
    }

    using assist::dft;
    using assist::idft;
    using assist::ifftshift;

    // calculate time domain ramlak filter
    std::vector<float> ramlak(filterSize, 0.f);
    ramlak[0] = .25f;
    for(auto shift = 1u; shift < filterSize/2; shift += 2)
    {
        ramlak[shift] = ramlak[filterSize - shift] = -1.0f / std::pow(shift * PI, 2);
    }
    ramlak[filterSize/2] = -1.0f / std::pow(filterSize/2 * PI, 2);
    
    // early return for unscaled ramlak filter
    if(_filterType == RamLak && _frequencyScaling >= 1.0f)
    {
        return ramlak;
    }

    // switch to frequency domain
    auto f_filter = dft(ramlak);
    auto freq = Range<double>::linspace(
        -PI/_frequencyScaling,
        PI/_frequencyScaling,
        filterSize,
        false);
    ifftshift(freq);

    // calculation of different apodization filters
    switch (_filterType)
    {
    case SheppLogan:
        for(auto i = 1u; i < filterSize; ++i)
        {
            f_filter[i] *= std::sin(freq[i])/freq[i];
        }
        break;

    case Cosine:
        for(auto i = 0u; i < filterSize; ++i)
        {
            f_filter[i] *= std::cos(0.5*freq[i]);
        }
        break;

    case Hann:
        for(auto i = 0u; i < filterSize; ++i)
        {
            f_filter[i] *= 0.5*(1. + std::cos(freq[i]));
        }
        break;

    case RamLak:
        // nothing to do
        break;

    default:
        throw std::runtime_error("ApodizationFilter::generateCoefficients: invalid filter type.");
    }

    // zero outside window
    if(_frequencyScaling < 1.f)
    {
        const auto cutoff = static_cast<int>(_frequencyScaling*f_filter.size()/2);
        std::fill(
            std::next(std::begin(f_filter), cutoff + 1),
            std::prev(std::end(f_filter), cutoff),
            0.f
        );
    }

    // back to time domain
    return idft(f_filter);
}

/*!
 * \brief QMetaEnum object used to decode/encode subset order enum values.
 */
QMetaEnum ApodizationFilter::metaEnum()
{
    const auto& mo = ApodizationFilter::staticMetaObject;
    const auto idx = mo.indexOfEnumerator("FilterType");
    return mo.enumerator(idx);
}

/*!
 * \brief Constructs an instance of RamLakFilter with multiplicative projection scaling by factor
 * \a scaling.
 */
RamLakFilter::RamLakFilter(float scaling)
    : GenericOCLProjectionFilter("processing/ramlak_filter.cl", { scaling })
{
}

} // namespace OCL

namespace assist {

std::vector<std::complex<double>> dft(const std::vector<float>& image)
{
    static constexpr std::complex<double> mpi2i{0., -2.*PI};
    const auto dsize = static_cast<double>(image.size());
    std::vector<std::complex<double>> fourier(image.size(), {0.f, 0.f});
    for(auto k = 0u; k < fourier.size(); ++k)
    {
        for(auto j = 0u; j < image.size(); ++j)
        {
            const auto cphase = mpi2i*(k*j/dsize);
            fourier[k] += static_cast<double>(image[j])*std::exp(cphase);
        }
    }
    return fourier;
}

std::vector<float> idft(const std::vector<std::complex<double>>& fourier)
{
    static constexpr std::complex<double> pi2i{0., 2.*PI};
    const auto dsize = static_cast<double>(fourier.size());
    std::vector<std::complex<double>> image(fourier.size(), {0.f, 0.f});
    for(auto k = 0u; k < image.size(); ++k)
    {
        for(auto j = 0u; j < fourier.size(); ++j)
        {
            const auto cphase = pi2i*(k*j/dsize);
            image[k] += fourier[j]*std::exp(cphase);
        }
        image[k] /= fourier.size();
    }
    return vector_complex_to_real(image);
}

} // namespace assist
} // namespace CTL
