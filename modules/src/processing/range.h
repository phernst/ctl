#ifndef CTL_RANGE_H
#define CTL_RANGE_H

#include <algorithm>
#include <cmath>
#include <stdexcept>
#include <vector>

typedef unsigned int uint;

namespace CTL {

// Range
/*!
 * \class Range
 * \brief The Range class holds start and end point of a value range (or interval) and provides a
 * few convenience methods to operate on that range.
 *
 * This class holds a start and end point (of same type) that are intended to describe an interval
 * bounded by these two values. Besides its use as a pair-like container in several places
 * throughout the CTL, the main purpose of this class lies in its convenience methods for creation
 * of a vector of sampling points across the specified interval. Two different sampling patterns
 * are available:
 * - linspace(), creates a vector with equally-spaced points
 * - expspace(), creates a vector with point of exponentially growing distances.
 *
 * The template type for this class needs to satisfy basic mathematical operations, such as +, -,
 * and /.
 */
template<typename T>
class Range
{
public:
    // ctor
    /*!
     * \brief Constructs a Range representing the interval [\a start, \a end].
     *
     * This creates a Range object and sets the borders of the interval to \a start and \a end,
     * respectively. Both \a start and \a end must be convertible to the type of Range.
     */
    template<typename T1, typename T2,
             typename = typename std::enable_if<std::is_convertible<T1, T>::value &&
                                                std::is_convertible<T2, T>::value>::type>
    Range(T1 start, T2 end) : _data{ static_cast<T>(start), static_cast<T>(end) }
    { }

    /*!
     * \brief Constructs a Range representing the interval [\a bounds.first, \a bounds.second].
     *
     * This creates a Range object and sets the borders of the interval to \a bounds.first and
     * \a bounds.second, respectively. The types of both elements in the pair must be convertible
     * to the type of Range.
     */
    template<typename T1, typename T2,
             typename = typename std::enable_if<std::is_convertible<T1, T>::value &&
                                                std::is_convertible<T2, T>::value>::type>
    Range(const std::pair<T1, T2>& bounds)
        : _data{ static_cast<T>(bounds.first), static_cast<T>(bounds.second) }
    { }

    static Range fromCenterAndWidth(T center, T width);

    // start and end of the range
    T& start();
    T& end();
    const T& start() const;
    const T& end() const;

    // center of the range
    T center() const;
    // width of the range
    T width() const;

    // spacing that a linspace vector would have, given a specific number of samples
    T spacing(uint nbSamples) const;

    // linspace
    std::vector<T> linspace(uint nbSamples, bool endpoint = true) const;
    std::vector<T> expspace(uint nbSamples, bool endpoint = true) const;
    // static linspace
    static std::vector<T> linspace(T from, T to, uint nbSamples, bool endpoint = true);
    static std::vector<T> expspace(T from, T to, uint nbSamples, bool endpoint = true);

    // conversion (convenience)
    std::pair<T, T> toPair() const;

private:
    // data member (start and end)
    T _data[2];
};

typedef Range<uint>  IndexRange;
typedef Range<float> SamplingRange;

/*!
 * \brief Creates a Range object describing an interval of width \a width centered at \a center.
 *
 * The resulting Range has the following start and end points:
 * - start: (\a center - \a width / 2)
 * - end: (\a center + \a width / 2)
 *
 * Mathematical operations are performed with the type of the Range.
 *
 * Example:
 * \code
 *  auto range = Range<float>::fromCenterAndWidth(2.5f, 5.0f);
 *  qInfo() << range.start() << "," << range.end();            // output: 0 , 5
 *
 *  // be careful with integer types and odd 'width'!
 *  auto range_int = Range<int>::fromCenterAndWidth(3, 5);
 *  qInfo() << range_int.start() << "," << range_int.end();    // output: 1 , 5 (Note: actual width is now 4; center remains as requested)
 * \endcode
 */
template<typename T>
Range<T> Range<T>::fromCenterAndWidth(T center, T width)
{
    return Range<T>(center - width / T(2), center + width / T(2));
}

/*!
 * \brief Returns the width of the range (i.e. end - start).
 *
 * Same as end() - start().
 */
template<typename T>
T Range<T>::width() const { return _data[1] - _data[0]; }

/*!
 * \brief Returns the spacing that two sampling points would have if this range were divided into
 * \a nbSamples equidistant sampling points.
 *
 * Returns zero for \a nbSamples <= 1.
 *
 * Example:
 * \code
 *  auto range = Range<float>(0.0f, 10.0f);
 *  qInfo() << range.spacing(5);            // output: 2.5
 * \endcode
 */
template<typename T>
T Range<T>::spacing(uint nbSamples) const
{
    return (nbSamples > 1) ? (_data[1] - _data[0]) / T(nbSamples - 1)
            : T(0);
}

/*!
 * \brief Returns a (modifiable) reference to the start point of this range.
 */
template<typename T>
T& Range<T>::start() { return _data[0]; }

/*!
 * \brief Returns a (modifiable) reference to the end point of this range.
 */
template<typename T>
T& Range<T>::end() { return _data[1]; }

/*!
 * \brief Returns the start point of this range.
 */
template<typename T>
const T& Range<T>::start() const { return _data[0]; }

/*!
 * \brief Returns the end point of this range.
 */
template<typename T>
const T& Range<T>::end() const { return _data[1]; }

/*!
 * \brief Returns the center point of this range.
 *
 * This is computed (using operations of this range's value type) as:
 * (start + end) / 2.
 *
 * Example:
 * \code
 *  auto range = Range<float>(0.0f, 2.5f);
 *  qInfo() << range.center();              // output: 1.25
 *
 *  auto range_int = Range<int>(0, 5);
 *  qInfo() << range_int.center();          // output: 2 (Note: computations in 'int')
 * \endcode
 */
template<typename T>
T Range<T>::center() const { return (_data[0] + _data[1]) / T(2); }

/*!
 * \brief Generates a vector with \a nbSamples points that are equally distributed across this range.
 *
 * If \a nbSamples > 1 and \a endpoint = true, the last sample is \ref end(). Otherwise, it
 * is not included in the returned result.
 *
 * Example:
 * \code
 *  auto range = Range<float>(0.0f, 2.5f);
 *  qInfo() << range.linspace(5);           // output: std::vector(0, 0.625, 1.25, 1.875, 2.5)
 *  qInfo() << range.linspace(5, false);    // output: std::vector(0, 0.5, 1, 1.5, 2)
 * \endcode
 */
template<typename T>
std::vector<T> Range<T>::linspace(uint nbSamples, bool endpoint) const
{
    return linspace(_data[0], _data[1], nbSamples, endpoint);
}

/*!
 * \brief Generates a vector with \a nbSamples points that are distributed across this range with
 * exponentially increasing distances.
 *
 * The start and end point of this range must be (strictly) larger than zero when using this method;
 * throws an std::domain_error otherwise.
 *
 * If \a nbSamples > 1 and \a endpoint = true, the last sample is \ref end(). Otherwise, it
 * is not included in the returned result.
 *
 * Example:
 * \code
 *  auto range = Range<float>(1.0f, 10.0f);
 *  qInfo() << range.expspace(5);           // output: std::vector(1, 1.77828, 3.16228, 5.62341, 10)
 *  qInfo() << range.expspace(5, false);    // output: std::vector(1, 1.58489, 2.51189, 3.98107, 6.30957)
 * \endcode
 */
template<typename T>
std::vector<T> Range<T>::expspace(uint nbSamples, bool endpoint) const
{
    return expspace(_data[0], _data[1], nbSamples, endpoint);
}

/*!
 * \brief Generates a vector with \a nbSamples points that are equally distributed across the range
 * of [\a from, \a to].
 *
 * If \a nbSamples > 1 and \a endpoint = true, the last sample is \a to. Otherwise, it
 * is not included in the returned result.
 *
 * Same as Range<T>(from, to).linspace(nbSamples, endpoint).
 *
 * Example:
 * \code
 * qInfo() << Range<float>::linspace(0.0f, 10.0f, 6);        // output: std::vector(0, 2, 4, 6, 8, 10)
 * qInfo() << Range<float>::linspace(0.0f, 10.0f, 6, false); // output: std::vector(0, 1.66667, 3.33333, 5, 6.66667, 8.33333)
 * \endcode
 */
template<typename T>
std::vector<T> Range<T>::linspace(T from, T to, uint nbSamples, bool endpoint)
{
    std::vector<T> ret(nbSamples);
    const T increment = (nbSamples > 1) ? (to - from) / T(nbSamples - endpoint)
                                        : T(0);
    uint idx = 0;
    std::generate(ret.begin(), ret.end(), [&idx, from, increment]
                                          { return from + T(idx++) * increment; });

    return ret;
}

/*!
 * \brief Generates a vector with \a nbSamples points that are distributed across the range
 * of [\a from, \a to] with exponentially increasing distances.
 *
 * \a from and \a to must be (strictly) larger than zero; throws an std::domain_error otherwise.
 *
 * If \a nbSamples > 1 and \a endpoint = true, the last sample is \a to. Otherwise, it
 * is not included in the returned result.
 *
 * Same as Range<T>(from, to).expspace(nbSamples, endpoint).
 *
 * Example:
 * \code
 * qInfo() << Range<float>::expspace(1.0f, 100.0f, 6);        // output: std::vector(1, 2.51189, 6.30957, 15.8489, 39.8107, 100)
 * qInfo() << Range<float>::expspace(1.0f, 100.0f, 6, false); // output: std::vector(1, 2.15443, 4.64159, 10, 21.54435, 46.41589)
 * \endcode
 */
template<typename T>
std::vector<T> Range<T>::expspace(T from, T to, uint nbSamples, bool endpoint)
{
    if(from <= T(0) || to <= T(0))
        throw std::domain_error("Range::expspace: Exponential sampling is not supported for "
                                "non-positive sampling points");

    const auto logVals = Range<double>::linspace(std::log(from), std::log(to), nbSamples, endpoint);
    std::vector<T> ret(logVals.size());

    std::transform(logVals.cbegin(), logVals.cend(), ret.begin(), [](double val) {
        constexpr double roundIfInt = std::is_integral<T>::value ? 0.5 : 0.0;
        return static_cast<T>(std::exp(val) + roundIfInt);
    });

    return ret;
}

/*!
 * \brief Returns the start and end point of the range as a std::pair.
 */
template<typename T>
std::pair<T, T> Range<T>::toPair() const
{
    return { _data[0], _data[1] };
}

} // namespace CTL

/*! \file */
///@{

/*!
 * \typedef CTL::IndexRange
 * \brief Alias for Range<unsigned int>.
 *
 * This Range type is typically used to describe ranges/intervals of indices.
 *
 * \relates CTL::Range
 */

/*!
 * \typedef CTL::SamplingRange
 * \brief Alias for Range<float>.
 *
 * This Range type is typically used to describe ranges/intervals of sampling points.
 *
 * \relates CTL::Range
 */

///@}

#endif // CTL_RANGE_H
