#ifndef CTL_ABSTRACTPROJECTIONFILTER_H
#define CTL_ABSTRACTPROJECTIONFILTER_H

#include "io/serializationinterface.h"

namespace CTL {

class ProjectionData;

class AbstractProjectionFilter : public SerializationInterface
{
    CTL_TYPE_ID(3000)

public:virtual void filter(ProjectionData& projections) = 0;

public:
    virtual ~AbstractProjectionFilter() = default;

    virtual QVariant parameter() const;
    virtual void setParameter(const QVariant& parameter);

    void fromVariant(const QVariant& variant) override;
    QVariant toVariant() const override;

protected:
    AbstractProjectionFilter() = default;
    AbstractProjectionFilter(const AbstractProjectionFilter&) = default;
    AbstractProjectionFilter(AbstractProjectionFilter&&) = default;
    AbstractProjectionFilter& operator=(const AbstractProjectionFilter&) = default;
    AbstractProjectionFilter& operator=(AbstractProjectionFilter&&) = default;
};

} // namespace CTL

#endif // CTL_ABSTRACTPROJECTIONFILTER_H
