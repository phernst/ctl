#include "genericoclvolumefilter.h"

#include "img/voxelvolume.h"
#include "ocl/clfileloader.h"
#include "ocl/openclfunctions.h"
#include "ocl/pinnedmem.h"

#include <QDebug>

namespace CTL {
namespace OCL {

DECLARE_SERIALIZABLE_TYPE(GenericOCLVolumeFilter)

/*!
 * \brief Creates a GenericOCLVolumeFilter that executes the OpenCL kernel \a kernel.
 *
 * For internal use only. Do not use this ctor directly---i.e. without an associated .cl file
 * holding the kernel code---as this would prevent serializability of the GenericOCLVolumeFilter
 * instance.
 */
GenericOCLVolumeFilter::GenericOCLVolumeFilter(cl::Kernel* kernel,
                                               const std::vector<float>& arguments)
    : _kernel(kernel)
    , _queue(getCommandQueue())
{
    if(!_kernel)
        throw std::runtime_error("GenericOCLVolumeFilter: Could not create filter. "
                                 "Reason: OpenCL Kernel is invalid.");

    setAdditionalKernelArgs(arguments);
}

/*!
 * \brief Creates a GenericOCLVolumeFilter instance that is capable of executing the OpenCL
 * kernel (`filter`) of the file \a clFileName.
 *
 * The \a clFileName (incl. path) is relative to the runtime cl_src folder of the CTL
 * (see ClFileLoader::openCLSourceDir()). More details can be found under 'Notes' in the detailed
 * class description).
 * For further details on the requirements for the kernel, please also refer to the detailed
 * class description.
 * Note that, if the kernel specified in \a clFileName has additional arguments, the same number of
 * arguments must be passed to \a arguments to make the filter work. Alternatively, additional
 * arguments may be passed later on through setAdditionalKernelArgs().
 */
GenericOCLVolumeFilter::GenericOCLVolumeFilter(const std::string& clFileName,
                                               const std::vector<float>& arguments)
    : GenericOCLVolumeFilter(addKernelFromFile(clFileName), arguments)
{
    _clFileName = clFileName;
}

/*!
 * \brief Creates a GenericOCLVolumeFilter without any OpenCL kernel associated to it.
 *
 * For the purpose of deserialization only.
 */
GenericOCLVolumeFilter::GenericOCLVolumeFilter()
    : _kernel(nullptr)
    , _queue(getCommandQueue())
{
}

/*!
 * \brief Returns the global worksize for the kernel call.
 *
 * By default, this returns a two-dimensional \c cl::NDRange, where the first and second dimension
 * are the number of voxels in x- and y-direction in \a volume, respectively.
 *
 * Re-implement in sub-classes to change this behavior.
 */
cl::NDRange GenericOCLVolumeFilter::globalWorksize(const VoxelVolume<float>& volume) const
{
    const auto X = volume.dimensions().x;
    const auto Y = volume.dimensions().y;
    return cl::NDRange(X, Y);
}

/*!
 * \brief Returns the local worksize for the kernel call.
 *
 * By default, this returns \c cl::NullRange. Re-implement in sub-classes to change this behavior.
 */
cl::NDRange GenericOCLVolumeFilter::localWorksize(const VoxelVolume<float>&) const
{
    return cl::NullRange;
}

/*!
 * \brief Adds the OpenCL kernel defined in the file \a clFileName to the OpenCL environment and
 * returns a pointer to the created cl::Kernel object.
 *
 * Note that \a clFileName (incl. path) is relative to the runtime cl_src folder of the CTL
 * (see ClFileLoader::openCLSourceDir()). See also ClFileLoader for more details.
 */
cl::Kernel* GenericOCLVolumeFilter::addKernelFromFile(const std::string& clFileName)
{
    static const std::string kernelName = "filter";
    const std::string programName = "GenericOCLVolumeFilter_" + clFileName;

    // load source code from file
    ClFileLoader clFile(clFileName);
    if(!clFile.isValid())
        throw std::runtime_error(clFileName + "\nis not readable");
    const auto clSourceCode = clFile.loadSourceCode();

    // add kernel to OCLConfig
    OpenCLConfig::instance().addKernel(kernelName, OpenCLFunctions::write_bufferf, clSourceCode,
                                       programName);

    // return kernel (compiles program on device if required)
    try
    {
        return OpenCLConfig::instance().kernel(kernelName, programName);
    }
    catch(const cl::Error& err)
    {
        if(err.err() == CL_INVALID_KERNEL_NAME)
            qCritical() << programName.c_str()
                        << ": Kernel name is invalid. "
                           "Make sure it is correctly named 'filter'.";
        else
            qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";

        return nullptr;
    }
}

/*!
 * \brief Convenience method for simpler setting of a single additional argument.
 *
 * This is a convenience method to be used only for kernels that have a single additional
 * argument.
 *
 * Same as: `setAdditionalKernelArgs( { argument } );`
 */
void GenericOCLVolumeFilter::setAdditionalKernelArg(float argument)
{
    setAdditionalKernelArgs( { argument } );
}

/*!
 * \brief Sets the additional kernel arguments to \a arguments.
 *
 * Each element of \a arguments is passed as an individual argument to the OpenCL kernel of this
 * instance. The index of these arguments starts at 3 (three fixed arguments + additional args from
 * \a arguments).
 *
 * Note that, if the kernel used by this instance has additional arguments, the same number of
 * arguments must be passed to \a arguments to make the filter work.
 */
void GenericOCLVolumeFilter::setAdditionalKernelArgs(const std::vector<float>& arguments)
{
    _kernel->setArg(0, dummyImage3D()); // required for compatibility with Intel OCL driver

    for(size_t arg = 0; arg < arguments.size(); ++arg)
        _kernel->setArg(uint(3 + arg), arguments[arg]);

    // remember arguments (to enable later serialization)
    _additionalArgs = arguments;
}

/*!
 * \brief Filters the input \a volume by executing the OpenCL kernel of this instance.
 *
 * This method will handle all OpenCL host code required to run the kernel of this instance with
 * the input data from \a volume and the additional kernel arguments specified in the constructor
 * or from the latest call to setAdditionalKernelArgs().
 *
 * Individual kernel calls are queued for each z-slice of the input volume. The global and local
 * worksizes for the kernel execution are taken from globalWorksize() and localWorksize(),
 * respectively.
 * The full input volume is transfered at once to the OpenCL device and remains there for all calls
 * of the kernel. It is represented as a \c cl::Image3D whose dimensions correspond to the
 * dimensions of \a volume. This allows, for example, for data access outside the currently
 * processed z-slice (e.g. to filter in that direction). Similarly, the output buffer has the full
 * size and stays on the device for the entire execution. A single result readout is performed at
 * the very end (i.e. after all z-slices have been processed) and the results are copied back to
 * \a volume.
 *
 * This implementation requires OpenCL image support, but no image write support.
 */
void GenericOCLVolumeFilter::filter(VoxelVolume<float>& volume)
{
    try // OpenCL exception handling
    {
        const auto& context = _queue.getInfo<CL_QUEUE_CONTEXT>();

        // create image objects
        const cl::size_t<3> origin{};
        cl::size_t<3> imgSize;
        imgSize[0] = volume.dimensions().x;
        imgSize[1] = volume.dimensions().y;
        imgSize[2] = volume.dimensions().z;

        cl::Image3D oldVolume(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY,
                              cl::ImageFormat(CL_INTENSITY, CL_FLOAT),
                              imgSize[0], imgSize[1], imgSize[2]);
        PinnedBufHostRead<float> newVolume(imgSize[0]*imgSize[1]*imgSize[2], _queue);

        // transfer volume data
        _queue.enqueueWriteImage(oldVolume, CL_FALSE, origin, imgSize, 0, 0, volume.rawData());

        // set arguments
        _kernel->setArg(0, oldVolume);
        _kernel->setArg(1, newVolume.devBuffer());

        // run kernel
        const auto globalWS = globalWorksize(volume);
        const auto localWS = localWorksize(volume);
        for(auto z = 0u, nbSlices = uint(imgSize[2]); z < nbSlices; ++z)
        {
            _kernel->setArg(2, z);
            _queue.enqueueNDRangeKernel(*_kernel, cl::NullRange, globalWS, localWS);
        }

        // read result
        newVolume.transferDevToPinnedMem();
        std::copy_n(newVolume.hostPtr(), imgSize[0]*imgSize[1]*imgSize[2], volume.rawData());

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";
    }
}

/*!
 * \brief Returns the parameters of this instance as QVariant.
 *
 * This returns a QVariantMap with two key-value-pairs:
 * - ("Kernel file name", file name of the .cl file containing the kernel),
 * - ("Additional arguments", list of the used additional kernel parameters).
 */
QVariant GenericOCLVolumeFilter::parameter() const
{
    QVariantMap ret = AbstractVolumeFilter::parameter().toMap();

    QVariantList parList;
    parList.reserve(static_cast<int>(_additionalArgs.size()));
    for(const auto& arg : _additionalArgs)
        parList.append(arg);

    ret.insert(QStringLiteral("Kernel file name"), QString::fromStdString(_clFileName));
    ret.insert(QStringLiteral("Additional arguments"), parList);

    return ret;
}

/*!
 * \brief Sets the parameters of this instance based on the passed QVariant \a parameter.
 *
 * Parameters must be passed as a QVariantMap with one or both of the follwing key-value-pairs:
 * - ("Kernel file name", file name of the .cl file containing the kernel),
 * - ("Additional arguments", list of the used additional kernel parameters).
 *
 * Note that it is not recommended to change settings of an GenericOCLVolumeFilter this way. This
 * methods mainly serves its purpose in deserialization.
 */
void GenericOCLVolumeFilter::setParameter(const QVariant& parameter)
{
    AbstractVolumeFilter::setParameter(parameter);

    const QVariantMap parMap = parameter.toMap();

    if(parMap.contains(QStringLiteral("Kernel file name")))
    {
        _clFileName = parMap.value(QStringLiteral("Kernel file name")).toString().toStdString();
        _kernel = addKernelFromFile(_clFileName);
    }
    if(parMap.contains(QStringLiteral("Additional arguments")))
    {
        auto argList = parMap.value(QStringLiteral("Additional arguments")).toList();
        std::vector<float> argVec;
        argVec.reserve(argList.size());
        for(const auto& arg : qAsConst(argList))
            argVec.push_back(arg.toFloat());

        setAdditionalKernelArgs(argVec);
    }
}

/*!
 * \brief Returns the command queue to be used by this instance.
 *
 * Note that this will always use the first OpenCL device found in
 * OpenCLConfig::instance().devices(). Configure the device list accordingly before instantiating
 * this class if you want to utilize a particular device.
 */
cl::CommandQueue GenericOCLVolumeFilter::getCommandQueue()
{
    try // OpenCL exception handling
    {
        const auto& config  = OpenCLConfig::instance();
        const auto& context = config.context();
        return cl::CommandQueue(context, config.devices().front());
    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";
    }

    return {};
}

} // namespace OCL
} // namespace CTL
