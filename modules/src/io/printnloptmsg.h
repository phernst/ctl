#ifndef CTL_PRINTNLOPTMSG_H
#define CTL_PRINTNLOPTMSG_H

#include <nlopt.hpp>
#include <QDebug>

namespace CTL {
namespace NLOPT {

inline void printNloptMsg(nlopt::result errCode)
{
    if(errCode < 0)
        qCritical("Optimization failed:");
    else if(errCode == nlopt::result::MAXEVAL_REACHED || errCode == nlopt::result::MAXTIME_REACHED)
        qWarning("Potential unintended termination of optimization (not converged).");

    switch(errCode)
    {
    // errors
    case nlopt::result::FAILURE:
        qCritical("Generic failure code.");
        break;
    case nlopt::result::INVALID_ARGS:
        qCritical("Invalid arguments (e.g. lower bounds are bigger than upper bounds, an unknown "
                  "algorithm was specified, etcetera).");
        break;
    case nlopt::result::OUT_OF_MEMORY:
        qCritical("Ran out of memory.");
        break;
    case nlopt::result::ROUNDOFF_LIMITED:
        qCritical("Halted because roundoff errors limited progress. (In this case, the "
                  "optimization still typically returns a useful result.)");
        break;
    case nlopt::result::FORCED_STOP:
        qCritical("Halted because of a forced termination: the user called nlopt_force_stop(opt) "
                  "on the optimization’s nlopt_opt object opt from the user’s objective function "
                  "or constraints.");
        break;

    // Successful termination
    case nlopt::result::SUCCESS:
        break;

    case nlopt::result::STOPVAL_REACHED:
        qDebug("Optimization stopped because stopval was reached.");
        break;

    case nlopt::result::FTOL_REACHED:
        qDebug("Optimization stopped because ftol_rel or ftol_abs was reached.");
        break;

    case nlopt::result::XTOL_REACHED:
        qDebug("Optimization stopped because xtol_rel or xtol_abs was reached.");
        break;

    case nlopt::result::MAXEVAL_REACHED:
        qDebug("Optimization stopped because maxeval was reached.");
        break;

    case nlopt::result::MAXTIME_REACHED:
        qDebug("Optimization stopped because maxtime was reached.");
        break;

    default:
        break;
    }
}

} // namespace NLOPT
} // namespace CTL

#endif // CTL_PRINTNLOPTMSG_H
