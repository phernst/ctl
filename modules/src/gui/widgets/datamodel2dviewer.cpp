#include "datamodel2dviewer.h"
#include "ui_datamodel2dviewer.h"

#include "processing/range.h"
#include "lineseriesview.h"
#include "intervalseriesview.h"

#include <qmath.h>

namespace CTL {
namespace gui {

/*!
 * Creates a DataModel2DViewer and sets its parent to \a parent.
 */
DataModel2DViewer::DataModel2DViewer(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::DataModel2DViewer)
{
    ui->setupUi(this);

    connect(ui->_SB_rangeFrom, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &DataModel2DViewer::updatePlot);
    connect(ui->_SB_rangeTo, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &DataModel2DViewer::updatePlot);
    connect(ui->_SB_rangeFromY, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &DataModel2DViewer::updatePlot);
    connect(ui->_SB_rangeToY, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &DataModel2DViewer::updatePlot);
    connect(ui->_PB_reduceSampling, &QPushButton::clicked, this, &DataModel2DViewer::reduceSamplingDensityX);
    connect(ui->_PB_increaseSampling, &QPushButton::clicked, this, &DataModel2DViewer::increaseSamplingDensityX);
    connect(ui->_SB_nbSamples, QOverload<int>::of(&QSpinBox::valueChanged), this, &DataModel2DViewer::updatePlot);
    connect(ui->_PB_reduceSamplingY, &QPushButton::clicked, this, &DataModel2DViewer::reduceSamplingDensityY);
    connect(ui->_PB_increaseSamplingY, &QPushButton::clicked, this, &DataModel2DViewer::increaseSamplingDensityY);
    connect(ui->_SB_nbSamplesY, QOverload<int>::of(&QSpinBox::valueChanged), this, &DataModel2DViewer::updatePlot);
    connect(ui->_W_parameterEditor, &ParameterConfigWidget::parameterChanged, this, &DataModel2DViewer::setModelParameter);
    connect(ui->_W_dataViewer, &Chunk2DView::pixelInfoUnderCursor, this, &DataModel2DViewer::updatePixelInfo);

    // connections for windowing
    connect(ui->_W_windowing, &WindowingWidget::windowingChanged, this, &DataModel2DViewer::windowingUpdate);
    connect(ui->_W_windowing, &WindowingWidget::autoWindowingRequested, ui->_W_dataViewer, &Chunk2DView::setWindowingMinMax);
    connect(ui->_W_dataViewer, &Chunk2DView::windowingChanged, ui->_W_windowing, &WindowingWidget::setWindowDataSilent);
    // connections for zoom
    connect(ui->_W_zoomControl, &ZoomControlWidget::zoomRequested, ui->_W_dataViewer, &Chunk2DView::setZoom);
    connect(ui->_W_dataViewer, &Chunk2DView::zoomChanged, ui->_W_zoomControl, &ZoomControlWidget::setZoomValueSilent);

    connect(ui->_comBox_colormap, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &DataModel2DViewer::changeColormap);

    ui->_W_dataViewer->setLivePixelDataEnabled(true);

    setWindowTitle("Data Model 2D Viewer");
}

/*!
 * Deletes this instance.
 */
DataModel2DViewer::~DataModel2DViewer()
{
    delete ui;
}

/*!
 * Returns a pointer to the Chund2DView child widget used for data display.
 */
Chunk2DView* DataModel2DViewer::chunkView() const
{
    return ui->_W_dataViewer;
}

/*!
 * Creates a DataModel2DViewer for \a model and shows the window.
 *
 * Note that this instance takes a copy of \a model through its clone() method.
 * \a model must not be nullptr; throws an std::runtime_error otherwise.
 *
 * The number of sampling points (in both dimensions) can be specified with \a nbSamples.
 *
 * The widget will be deleted automatically if the window is closed.
 *
 * Example:
 * \code
 *  // create a two-dimensional Gaussian model
 *  auto model = std::make_shared<GaussianModel2D>();
 *
 *  // visualize
 *  gui::DataModel2DViewer::plot(model, 300);
 * \endcode
 */
void DataModel2DViewer::plot(std::shared_ptr<AbstractDataModel2D> model, uint nbSamples)
{
    if(!model)
        throw std::runtime_error("DataModel2DViewer::plot: model must not be nullptr.");

    plot(*model, nbSamples);
}

/*!
 * Creates a DataModel2DViewer for \a model and shows the window.
 *
 * Note that this instance takes a copy of \a model through its clone() method.
 *
 * The number of sampling points (in both dimensions) can be specified with \a nbSamples.
 *
 * The widget will be deleted automatically if the window is closed.
 *
 * Example:
 * \code
 *  // create a two-dimensional Gaussian model
 *  GaussianModel2D model;
 *
 *  // visualize
 *  gui::DataModel2DViewer::plot(model, 300);
 * \endcode
 */
void DataModel2DViewer::plot(const AbstractDataModel2D& model, uint nbSamples)
{
    auto viewer = new DataModel2DViewer;
    viewer->setAttribute(Qt::WA_DeleteOnClose);

    viewer->setData(model);
    viewer->setNumberOfSamplesX(nbSamples);
    viewer->setNumberOfSamplesY(nbSamples);

    viewer->chunkView()->setAutoMouseWindowScaling();

    viewer->resize(800, 600);
    viewer->show();
}

/*!
 * Sets the colormap used to visualize data to one of the predefined colormaps:
 * 0 - grayscale
 * 1 - HSV table
 * 2 - jet table
 *
 * Example:
 * \code
 * auto gaussian = std::make_shared<GaussianModel2D>(42.0f, 0.0f, 2.0f);
 *
 * auto viewer = new gui::DataModel2DViewer; // needs to be deleted at an appropriate time
 * viewer->setData(gaussian);
 * viewer->resize(750,400);
 * viewer->show();
 *
 * // change colormap to HSV (mapIdx = 1)
 * viewer->changeColormap(1);
 * \endcode
 *
 * Note that the same functionality is provided in the GUI.
 */
void DataModel2DViewer::changeColormap(int mapIdx)
{
    switch(mapIdx)
    {
    case 0:
        ui->_W_dataViewer->setColorTable(assist::colortableGrayscale());
        break;
    case 1:
        ui->_W_dataViewer->setColorTable(assist::colortableHue());
        break;
    case 2:
        ui->_W_dataViewer->setColorTable(assist::colortableJet());
        break;
    default:
        ui->_W_dataViewer->setColorTable(assist::colortableGrayscale());
    }

    // outside set request -> select correct index in the combo box widget
    if(mapIdx != ui->_comBox_colormap->currentIndex())
    {
        ui->_comBox_colormap->blockSignals(true);
        ui->_comBox_colormap->setCurrentIndex(mapIdx);
        ui->_comBox_colormap->blockSignals(false);
    }
}

/*!
 * Sets the data model visualized by this instance to \a model.
 *
 * Note that this will clone the model, such that the viewer instance will own a copy of \a model.
 * \a model must not be nullptr; throws an std::runtime_error otherwise.
 */
void DataModel2DViewer::setData(std::shared_ptr<AbstractDataModel2D> model)
{
    if(!model)
        throw std::runtime_error("DataModel2DViewer::setData: model must not be nullptr.");

    setData(*model);
}

/*!
 * Sets the data model visualized by this instance to \a model.
 *
 * Note that this will clone the model, such that the viewer instance will own a copy of \a model.
 */
void DataModel2DViewer::setData(const AbstractDataModel2D& model)
{
    _model.reset(model.clone());

    ui->_W_parameterEditor->updateInterface(_model->parameter());
    ui->_W_parameterEditor->isEmpty() ? ui->_GB_parameter->hide() : ui->_GB_parameter->show();

    updatePlot();
}

// public slots

/*!
 * Increases the number of sampling points in x direction by 25% of their current value.
 */
void DataModel2DViewer::increaseSamplingDensityX()
{
    setNumberOfSamplesX(qCeil(ui->_SB_nbSamples->value() * 1.25));
}

/*!
 * Increases the number of sampling points in y direction by 25% of their current value.
 */
void DataModel2DViewer::increaseSamplingDensityY()
{
    setNumberOfSamplesY(qCeil(ui->_SB_nbSamplesY->value() * 1.25));
}

/*!
 * Hides the model parameter GUI element if \a hide = \c true and shows it otherwise.
 */
void DataModel2DViewer::hideParameterGUI(bool hide)
{
    hide ? ui->_GB_parameter->hide() : ui->_GB_parameter->show();
}

/*!
 * Reduces the number of sampling points in x direction to 80% of their current value.
 */
void DataModel2DViewer::reduceSamplingDensityX()
{
    setNumberOfSamplesX(qCeil(ui->_SB_nbSamples->value() * 0.8));
}

/*!
 * Reduces the number of sampling points in y direction to 80% of their current value.
 */
void DataModel2DViewer::reduceSamplingDensityY()
{
    setNumberOfSamplesY(qCeil(ui->_SB_nbSamplesY->value() * 0.8));
}

/*!
 * Sets the number of sampling points in x direction to \a nbSamples.
 */
void DataModel2DViewer::setNumberOfSamplesX(int nbSamples)
{
    ui->_SB_nbSamples->setValue(nbSamples);
}

/*!
 * Sets the number of sampling points in y direction to \a nbSamples.
 */
void DataModel2DViewer::setNumberOfSamplesY(int nbSamples)
{
    ui->_SB_nbSamplesY->setValue(nbSamples);
}

/*!
 * Sets the x-range within which the model is sampled to [\a from, \a to].
 */
void DataModel2DViewer::setSamplingRangeX(float from, float to)
{
    ui->_SB_rangeFrom->setValue(from);
    ui->_SB_rangeTo->setValue(to);
}

/*!
 * Sets the y-range within which the model is sampled to [\a from, \a to].
 */
void DataModel2DViewer::setSamplingRangeY(float from, float to)
{
    ui->_SB_rangeFromY->setValue(from);
    ui->_SB_rangeToY->setValue(to);
}

/*!
 * Updates the current plot. This will readout all UI elements for information on sampling and
 * performs a new sampling of the values from the model.
 *
 * This is called automatically when necessary.
 */
void DataModel2DViewer::updatePlot()
{
    const auto nbSamplesX = ui->_SB_nbSamples->value();
    const auto nbSamplesY = ui->_SB_nbSamplesY->value();
    const auto rangeX = SamplingRange(ui->_SB_rangeFrom->value(), ui->_SB_rangeTo->value());
    const auto rangeY = SamplingRange(ui->_SB_rangeFromY->value(), ui->_SB_rangeToY->value());

    ui->_W_dataViewer->setData(_model->sampleChunk(rangeX, rangeY, nbSamplesX, nbSamplesY));

    if(ui->_CB_autoMinMax->isChecked())
        ui->_W_dataViewer->setWindowingMinMax();
}

void DataModel2DViewer::keyPressEvent(QKeyEvent *event)
{
    if(event->modifiers() == Qt::CTRL && event->key() == Qt::Key_S)
    {
        ui->_W_dataViewer->saveDialog();

        event->accept();
    }

    QWidget::keyPressEvent(event);
}

void DataModel2DViewer::setModelParameter(QVariant parameter)
{
    _model->setParameter(parameter);
    updatePlot();
}

void DataModel2DViewer::updatePixelInfo(int x, int y, float value)
{
    QString info = QStringLiteral("(") + QString::number(x) + QStringLiteral(" , ")
                                       + QString::number(y) + QStringLiteral("): ")
                                       + QString::number(value);
    ui->_L_pixelInfo->setText(info);
}

void DataModel2DViewer::windowingUpdate()
{
    auto newWindowing = ui->_W_windowing->windowFromTo();
    ui->_W_dataViewer->setWindowing(newWindowing.first, newWindowing.second);
}


} // namespace gui
} // namespace CTL
