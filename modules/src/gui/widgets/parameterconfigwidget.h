#ifndef CTL_PARAMETERCONFIGWIDGET_H
#define CTL_PARAMETERCONFIGWIDGET_H

#include <QVariant>
#include <QWidget>

class QGridLayout;

namespace CTL {
namespace gui {

class ParameterConfigWidget : public QWidget
{
    Q_OBJECT

public:
    ParameterConfigWidget(QWidget* parent = nullptr);

    bool isEmpty() const;

public slots:
    void updateInterface(QVariant templateParameter);

private:
    QGridLayout* _layout;

    void clearLayout();
    static QVariant parsedInputWidget(QWidget* widget);

signals:
    void parameterChanged(QVariant parameter);

private slots:
    void somethingChanged();
};

} // namespace gui
} // namespace CTL

#endif // CTL_PARAMETERCONFIGWIDGET_H
